<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('lang/{lang}',[
    'as'=>'lang',
    'uses'=>'Frontend\LocaleController@lang'
]);

Route::get('/', [
  'as' => 'home',
  'uses' => 'Frontend\ShopController@getIndex'
//   function () {
//     return view('frontend.index');
// }
]);
// Route::get('/for-restaurant', [
//   'as' => 'for-restaurant',
//   'uses' => 'Frontend\ShopController@getIndex'
// ]);
Route::get('/for-customer', [
  'as' => 'for-customer',
  'uses' => 'Frontend\ShopController@getCustomer'
]);

Route::get('/thank-you', [
  'as' => 'thank-you',
  function () {
    return view('frontend.thankyou');
}
]);

Route::group(['middleware'=>'web','prefix' => 'shop'], function () {
    Route::get('register', [
      'as' => 'register',
      'uses' => 'Frontend\ShopController@getIndex'
    ]);
Route::post('register/check','Frontend\ShopController@postCheck');
});

/* Login Waiter */
Route::group(['middleware'=>'web','prefix' => 'waiter'], function () {
    Route::controller('login', 'Frontend\LoginWaiterController');

});
/* --------------------- API ------------------- */
Route::group(['prefix' => 'api'], function () {
    Route::controller('notify','API\NotifyController');
    Route::controller('menu','API\MenuController');
    Route::get('qr/{random_key}','API\QrController@qrCode');
    Route::get('category/{category_id}','API\CategoryController@Category');
    Route::get('menu_id/{memu_id}','API\MenuController@Menu');
    Route::get('menu_detail/{memu_id}','API\MenuDetailController@MenuDetail');
    Route::get('discount/{shop_id}/{code}','API\DiscountController@Discount');
});

/* --------------------- Admin ------------------- */
Route::group(['prefix' => '_admin'], function () {
    Route::controller('login', 'Backend\LoginController');
    Route::get('logout', 'Backend\LoginController@logout');

    Route::post('api-active','Backend\ApiController@changeActive');
    Route::post('api-site','Backend\ApiController@changeSite');
    Route::post('api-product','Backend\ApiController@changeProduct');
});
Route::group(['middleware'=>'admin','prefix' => config()->get('constants.BO_NAME')], function () {
    Route::get('/', function () { return view('backend.index'); });
    Route::resource('user-management','Backend\AdminController');
    Route::resource('role','Backend\AdminRoleController');
    Route::resource('page','Backend\AdminPageController');
    Route::resource('category','Backend\CategoryController');
    Route::resource('shop','Backend\ShopController');
    Route::resource('tables','Backend\TablesController');
    Route::resource('branch','Backend\BranchController');
    Route::resource('orders','Backend\OrdersController');
    Route::resource('option_group','Backend\OptionGroupController');
    Route::resource('option_value','Backend\OptionValueController');
    Route::resource('orders','Backend\OrdersController');
    Route::resource('orders_detail','Backend\OrdersDetailController');
    Route::resource('orders_detail_menu','Backend\OrdersDetailMenuController');
    Route::resource('orders_detail_option','Backend\OrdersDetailOptionController');
    Route::resource('banner_promotion','Backend\BannerPromotionController');
    Route::resource('waiter','Backend\WaiterTestController');
    Route::resource('menu','Backend\MenuController');
    Route::resource('set-permission','Backend\PermissionController');
    Route::resource('menu_option','Backend\MenuOptionController');
    Route::resource('call_log','Backend\CallLogController');
    Route::resource('owner','Backend\OwnerController');
    Route::resource('owner_log','Backend\OwnerLogController');
    Route::resource('change_password','Backend\ChangePasswordController');

    Route::post('del_tables','Backend\TablesController@postDeleteTable');
    Route::post('check-username','Backend\CheckUsernameController@checkuser');
    Route::post('tableno','Backend\WaiterController@postTableNo');
    Route::post('finishtime','Backend\CallLogController@postFinishTime');
    Route::resource('discount_code', 'Backend\DiscountCodeController');
    Route::resource('page','Backend\PageController');
    Route::controller('/sequence','Backend\SequenceController');
    Route::resource('kitchen_user', 'Backend\KitchenController');
    Route::resource('waiter_user', 'Backend\WaiterController');
    Route::post('check-kitchen','Backend\CheckKitchenController@checkuser');
    Route::post('check-waiter','Backend\CheckWaiterController@checkuser');
});
/* --------------------- Theme ------------------- */
Route::group(['prefix' => '_theme'], function () {
    Route::get('/',function(){
        return view('backend.theme_component.blank');
    });
    Route::get('form',function(){
        return view('backend.theme_component.form');
    });
    Route::get('list',function(){
        return view('backend.theme_component.list');
    });
});
