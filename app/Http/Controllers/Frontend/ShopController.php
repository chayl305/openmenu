<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\admin;
use App\Models\Shop;
use App\Models\ShopTr;
use App\Models\Owner;
use App\Models\Branch;
use App\Models\BranchTr;
use App\Models\Waiter;
use App\Models\Tables;
use App\Models\Kitchen;
use App\Models\BusinessType;
use App\Models\BusinessTypeTr;






use Input;
use Validator;
use Hash;
use Mail;

class ShopController extends Controller
{
    public function __construct(){

      $this->obj_fn = new MainFunction(); // Obj Function

      $this->lang = session()->get('locale');
    }
    public function getIndex(){
      $lang = $this->lang;
      $business_type = BusinessTypeTr::where('lang', $lang)->get();

      $test= 'index';
      return view('frontend.index', compact('business_type','test'));
    }
    public function getCustomer(){
      $lang = $this->lang;
      $business_type = BusinessTypeTr::where('lang', $lang)->get();

      $test= 'index';
      return view('frontend.for_customer', compact('business_type','test'));
    }

    public function postCheck(Request $request){
      // return view('frontend.email.verify', compact('adminDB','shopDB'));
      // return view('frontend.email.verify', compact('',''));
      $email = '';
      $obj_fn = new MainFunction();

      // return view('frontend.index', compact(''));

      //Shop Info
      // return $request->all();
      $shop_name_th = $request->shop_name_th;
      $shop_name_en = $request->shop_name_en;
      $business_type = $request->business_type;
      $username = $request->username;
      $password = $request->password;
      $number_of_table = $request->number_of_table;
      // company Info
      $company_name = $request->company_name;
      $company_address = $request->company_address;
      $company_phone = $request->company_phone;
      $company_email = $request->company_email;
      $company_website = $request->company_website;
      //contact Info
      $contact_name = $request->contact_name;
      $contact_phone = $request->contact_phone;
      $contact_email = $request->contact_email;

      $user = admin::where('username', $username)->first();
      if(!empty($user)){
        $error = 'username is duplicate.';
        return $error;
      }else{
        $hashed_password = Hash::make($password);
        $input_owner = [
          "company_name" => $company_name,
          "address" => $company_address,
          "phone" => $company_phone,
          "email" => $company_email,
          "max_shop" => 1
        ];
        $user = Owner::create($input_owner);

        $input_admin = [
          "admin_role_id" => 2,
          "username" => $username,
          "password" => $password,
          "owner_id" => $user['owner_id'],
          "firstname" => $contact_name,
          "lastname" => 'Owner'
        ];
        $adminDB = admin::create($input_admin);

        //gen kitchen user and password
        // $kitchen_user = $obj_fn->gen_random(6,1);
        // $kitchen_password = $obj_fn->gen_random(6,1);

        $input_shop = [
          "owner_id" => $user['owner_id'],
          "business_type_id" => $business_type,
          // "website" => $website,
          "address" => $company_address,
          "website" => $company_website,
          "contact_name" => $contact_name,
          "contact_phone" => $contact_phone,
          "contact_email" => $contact_email,
          "is_available" => 1
        ];
        $shopDB = Shop::create($input_shop);
        $input_shop_tr_th = [
          "shop_id" => $shopDB['shop_id'],
          "shop_name" => $shop_name_th,
          "lang" => 'th'
        ];
        $input_shop_tr_en = [
          "shop_id" => $shopDB['shop_id'],
          "shop_name" => $shop_name_en,
          "lang" => 'en'
        ];
        $shopTrDB = ShopTr::create($input_shop_tr_th);
        $shopTrDB = ShopTr::create($input_shop_tr_en);
        $name = ['th' => $company_name,'en' => $company_name];
        $gen_branch = $obj_fn->gen_branch($shopDB['shop_id'], $name);
        $gen_waiter = $obj_fn->gen_waiter($shopDB['shop_id']);
        $gen_kitchen = $obj_fn->gen_kitchen($shopDB['shop_id']);
        $gen_table = $obj_fn->gen_table($shopDB['shop_id'], $number_of_table);


        // Send Mail
        Mail::send('frontend.email.verify', [ 'adminDB' => $adminDB, 'shopDB' => $shopDB, 'gen_branch' => $gen_branch, 'gen_waiter' => $gen_waiter, 'gen_kitchen' => $gen_kitchen],
          function ($m) use ($email) {
              $m->to($email);
              $m->from('chayangkul@bangkoksolutions.com');
              $m->subject('Thank you for register on website');
          });
      // return view('frontend.email.verify', compact('adminDB','shopDB','gen_branch','gen_waiter','gen_kitchen'));
      return view('frontend.thankyou', compact(''));
      }
      // $test = '123';


      // return view('frontend.index', compact(''));
// >>>>>>> dev
    }
}
