<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\admin;
use App\Models\Branch;
use App\Models\Waiter;

use Input;
use Validator;
use Hash;

class LoginWaiterController extends Controller
{
    public function __construct(){

      $this->obj_fn = new MainFunction(); // Obj Function
    }
    public function getIndex(){

      return view('frontend.waiter.login', compact(''));
    }

    public function postCheck(Request $request){
      // return $request->email_login;
      $obj_fn = new MainFunction();
      $error = '';
      $username_login = $request->username_login;
      $password = $request->password;


      // $validator = Validator::make($request->all(),
      //   [
      //     "username_login" => 'required|email',
      //     "password" => 'required|between:6,20',
      //   ]
      // );
      // if($validator->fails()) {
      //       return redirect()->back()->withErrors($validator)->withInput();
      // }
      //Check user
      $waiter = Waiter::where('waiter_user', $username_login)->first();
      if(empty($waiter)){
        $error = 'not found user for waiter';
      }else{
        if(!Hash::check($password, $waiter->waiter_password)){
          $error = 'wrong password';
        }else{
          session()->put('waiter_name', $waiter->waiter_user);
          return view('frontend.waiter.wait_for_call');
        }

        return redirect()->back()->with('errorMsg',$error);
      }



      // return view('frontend.shop.register', compact(''));
    }

    // public function getWaitForCall(){
    //
    //   return view('frontend.waiter.wait_for_call', compact(''));
    // }

}
