<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Menu;
use App\Models\MenuTr;
use App\Models\MenuOption;
use App\Models\OptionGroup;
use App\Models\OptionGroupTr;
use App\Models\OptionValue;
use App\Models\OptionValueTr;

class MenuDetailController extends Controller
{
    public function MenuDetail($menu_id)
    {
        $menu = Menu::where('menu_id',$menu_id)->first();
        if(empty($menu)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Menu',
            ];
            return response()->json($result);
        }

        $menu_datil = MenuOption::where('menu_id',$menu_id)->first();
        if(empty($menu_datil)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Option',
            ];
            return response()->json($result);
        }

        $menuTr = MenuTr::where('menu_id',$menu_id)->get();
        $menu['menu_name'] = $menuTr;
        $memu_option = MenuOption::where('menu_id',$menu_id)->get();

        $result = [
            'status'        => '00',
            'statusTxt'     => 'Success',
            'menu'          => $menu,
            'option'        => $memu_option,
        ];
        return response()->json($result);
    }
}
