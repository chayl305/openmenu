<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\CallLog;
use App\Models\Tables;

class NotifyController extends Controller
{

  public function getNotify($table_no, $branch_id, $type, $random_key){
    if($type == 1 || $type == 2){

      $tables = Tables::where('random_key', $random_key)->first();
      // return $tables['table_no'];
        if($tables['table_no'] == $table_no && $tables['branch_id'] == $branch_id ){
          $time_now = new Carbon();
          $input_call_log = [
            "tables_id" => $tables['tables_id'],
            "action_id" => $type,
            "time_call" => $time_now,

          ];
          $call_log = CallLog::create($input_call_log);
          $status = [
            'status' => 'success',
            'description' => 'send request'
          ];
          return response()->json($status);
        }else{
          $status = [
            'status' => 'error',
            'description' => 'error request'
          ];
          return response()->json($status);
        }

    }else if($type == 3){
      $call_log = Tables::join('call_log', 'tables.tables_id', '=', 'call_log.tables_id');
      $call_log = $call_log->where('call_log.finish_time', '0000-00-00 00:00:00');
      $call_log = $call_log->where('tables.random_key', $random_key);
      $call_log = $call_log->where('tables.branch_id', $branch_id);
      $call_log_result = $call_log->get();

      foreach($call_log_result as $result){
        $time_now = new Carbon();
        $input_call_log = [
          "finish_time" => $time_now,
        ];
        $data = CallLog::find($result['call_log_id'])->update($input_call_log);

      }
      $status = [
        'status' => 'success',
        'description' => 'success request',
        'data' => $data
      ];
      return response()->json($status);

    }else{
      return response()->json(['status'=> 'invalid']);
    }
  }
}
