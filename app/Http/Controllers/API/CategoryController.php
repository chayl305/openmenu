<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shop;
use App\Models\ShopTr;
use App\Models\Category;
use App\Models\CategoryTr;

class CategoryController extends Controller{
	// public function __contruct(){
	// 	$this->lang = session()->get('local');

	// }
	public function Category($shop_id)
	{

		$cat = Category::where('shop_id',$shop_id)->first();
		if(empty($cat)){
			$result=[
			'status' => '01',
			'statusTxt' => 'Not Found Site',
			];
			return response()->json($result);
		}
		$category_id = $cat->category_id;
		$catTr = CategoryTr::where('category_id',$category_id)->get();
		$cat['category_name'] = $catTr;
		$result=[
		'status'  => '00',
		'statusTxt' =>'Success',
		'cat'    => $cat,

		];
		return response()->json($result);

	}
}