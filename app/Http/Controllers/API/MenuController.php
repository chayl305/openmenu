<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Menu;
use App\Models\MenuTr;
use App\Models\MenuOption;
use App\Models\OptionGroup;
use App\Models\OptionGroupTr;
use App\Models\OptionValue;
use App\Models\OptionValueTr;

class MenuController extends Controller
{
    public function __construct(){
          $this->lang = session()->get('locale');
    }
    public function getByMenu($menu_id){
        $menu = $this->getMenuList($menu_id ,null, null);

        return response()->json($menu);
    }
    public function getBySHop($shop_id){
        $menu = $this->menuList(null, $shop_id, null);

        return response()->json($menu);
    }
    public function getByCategory($category_id){
        $menu = $this->menuList(null, null, $category_id);

        return response()->json($menu);
    }

    public function getDetail($menu_id){
      $lang = $this->lang;
      $menu = Menu::find($menu_id);
      $options = null;
      if(!empty($menu)){
          // image path
          $img_path = config()->get('constants.NO_PIC');
          $img_dir = config()->get('constants.PATH_IMG_PRODUCT');
          $img_name = $menu->img_name;
          $file = $img_dir.$img_name;
          if(!empty($img_name) && file_exists($file)){
              $img_path = url()->asset($file);
          }
          $menu['image'] = $img_path;

          //menu option_group
          $menu_options = MenuOption::where('menu_id', $menu_id)->get();
          foreach($menu_options as $menu_option){
            $option_group = OptionGroup::where('option_group_id', $menu_option->option_group_id)->first();
            $option_group_tr = OptionGroupTr::where('option_group_id', $menu_option->option_group_id)->get();
            $option_value = OptionValue::where('option_group_id', $menu_option->option_value_id)->first();
            $option_value_tr = OptionValueTr::where('option_group_id', $menu_option->option_value_id)->get();

            $option_group['option_group_tr'] = $option_group_tr;
            $menu_option['option_group'] = $option_group;

            $option_value['option_value_tr'] = $option_value_tr;
            $menu_option['option_value'] = $option_value;

          }
          $menu['menu_options'] = $menu_options;
          return $menu;
      }

    }
    public function getMenuList($menu_id = null, $shop_id = null, $category_id = null){
      $lang = $this->lang;
      $menus = Menu::where('is_available', '1');
      // return "menu";
      if(!empty($menu_id)){
        $menus = $menus->where('menu_id', $menu_id);
      }
      if(!empty($shop_id)){
        $menus = $menus->where('shop_id', $shop_id);
      }
      if(!empty($category_id)){
        $menus = $menus->where('category', $category_id);
      }
      $menus = $menus->get();

      if(count($menus) > 0){
        foreach($menus as $menu){
              $img_path = config()->get('constants.NO_PIC');
              $img_dir = config()->get('constants.PATH_IMG_PRODUCT');
              $img_name = $menu->img_name;
              $file = $img_dir.$img_name;
              if(!empty($img_name) && file_exists($file)){
                    $img_path = url()->asset($file);
              }
              $menu['image'] = $img_path;

              $menu_tr = MenuTr::where('menu_id', $menu->menu_id)->get();
              $menu['lang'] = $menu_tr;
        }
      }
      // $result = ['menu' => $menus];
      $result = $menus;
      return $result;
    }
    public function Menu($category_id)
    {
      $menu = Menu::where('category_id',$category_id)->first();

        if(empty($menu)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Category',
            ];
            return response()->json($result);
        }

        $menuId = $menu->menu_id;
        $menuTr = MenuTr::where('menu_id',$menuId)->get();
        $menu['menu_name'] = $menuTr;

        $result = [
            'status'        => '00',
            'statusTxt'     => 'Success',
            'menu'          => $menu,
        ];
        return response()->json($result);
    }

}
