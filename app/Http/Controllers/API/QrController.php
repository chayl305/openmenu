<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Tables;
use App\Models\Shop;
use App\Models\ShopTr;
use App\Models\Branch;

class QrController extends Controller
{
    public function __construct(){
          $this->lang = session()->get('locale');
    }

    public function qrCode($random_key){
        $table = Tables::where('random_key',$random_key)->first();

        if(empty($table)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Table',
            ];
            return response()->json($result);
        }

        $branch_id = $table->branch_id;
        $table_id = $table->tables_id;
        $table_no = $table->table_no;

        $branch = Branch::where('branch_id',$branch_id)->where('is_available','1')->first();
        if(empty($branch)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Zone',
            ];
            return response()->json($result);
        }

        $shop_id = $branch->shop_id;
        $shop = Shop::where('shop_id',$shop_id)->where('is_available','1')->first();

        if(empty($shop)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Site',
            ];
            return response()->json($result);
        }

        $shopTr = ShopTr::where('shop_id',$shop_id)->get();
        $shop['shop_name'] = $shopTr;

        $result = [
            'status'        => '00',
            'statusTxt'     => 'Success',
            'shop'          => $shop,
            'branch_id'     => $branch_id,
            'tables_id'     => $table_id,
            'tables_no'     => $table_no
        ];


        return response()->json($result);
    }
}
