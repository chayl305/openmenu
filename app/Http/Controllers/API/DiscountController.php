<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Shop;
use App\Models\ShopTr;
use App\Models\DiscountCode;
use App\Models\DiscountCodeTr;

class DiscountController extends Controller
{
    public function Discount($shop_id, $code)
    {
        $discount_code = DiscountCode::where('code',$code)->first();
        if(empty($discount_code)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Discout Code',
            ];
            return response()->json($result);
        }

        $shop = explode(",",$discount_code->shop);
        $shopId = '' ;
        foreach ($shop as $key => $value) {
            if($value == $shop_id){
                $shopId = $value;
            }
        }

        if(empty($shopId)){
            $result = [
                'status'        => '01',
                'statusTxt'     => 'Not Found Shop',
            ];
            return response()->json($result);
        }

        $discount_code_id = $discount_code->discount_code_id;
        $discountTr = DiscountCodeTr::where('discount_code_id',$discount_code_id)->get();
        $discount_code['code_name'] = $discountTr;

        $result = [
            'status'        => '00',
            'statusTxt'     => 'Success',
            'discount'          => $discount_code,
        ];
        return response()->json($result);
    }
}
