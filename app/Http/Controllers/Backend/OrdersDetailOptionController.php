<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Branch;
use App\Models\Language;

use Input;
use Hash;
use DB;

class OrdersDetailOptionController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\OrdersDetailOption'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Orders Detail Option'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/orders_detail_option'; // Url Path
        $this->view_path = 'backend.orders_detail_option.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $orders_id = input::get('orders_id');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        
        $data = $obj_model->get();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','data','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {

    }
}
