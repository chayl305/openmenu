<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Language;
use App\Models\Menu;
use App\Models\MenuTr;

use Input;
use Hash;
use DB;

class OrdersDetailController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\OrdersDetail'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Orders Detail'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/orders_detail'; // Url Path
        $this->view_path = 'backend.orders_detail.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $orders_id = Input::get('orders_id');
        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language
        $data = $obj_model;
        $data = $data::leftJoin('orders','orders.orders_id','=','orders_detail.orders_id')
                ->select(DB::raw('orders_detail.*'),'orders.discount_price');
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(!empty($orders_id)){
                    $ownerId = Orders::leftJoin('shop', 'orders.shop_id', '=', 'shop.shop_id')
                                ->where('orders.orders_id', $orders_id)->first()->owner_id;
                    if (session()->get('s_owner_id') <> $ownerId) {
                        return abort(503);
                    }
                }
            }
        }
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {   
                if (!empty($orders_id))
                {   
                    $shopId = Orders::where('orders_id', $orders_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/
        if (!empty($orders_id))
        {
            $data = $obj_model->where('orders_id', $orders_id);
        }else{
            return abort(503);
        }
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->groupBy('orders_detail.'.$primaryKey);
        $data = $data->get();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $orders_id = Input::get('orders_id');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $orders_id = Input::get('orders_id');
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(!empty($orders_id)){
                    $ownerId = Orders::leftJoin('shop', 'orders.shop_id', '=', 'shop.shop_id')
                                ->where('orders.orders_id', $orders_id)->first()->owner_id;
                    if (session()->get('s_owner_id') <> $ownerId) {
                        return abort(503);
                    }
                }
            }
        }
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {   
                if (!empty($orders_id))
                {   
                    $shopId = Orders::where('orders_id', $orders_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/
        $order = Orders::Leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id');

        if (!empty($orders_id))
        {
            $order = $order->where('orders.orders_id', $orders_id);
        }else{
            return abort(503);
        }

        $order = $order->get();

        $data = $obj_model->get();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','data','obj_model','obj_fn','order'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {

    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {

    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {

    }
}
