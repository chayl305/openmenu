<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\PainCategory;
use App\Models\Tables;

use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class SequenceController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }
    public function postTable(Request $request)
    {
        $tables_id = $request->tables_id;
        $branch_id = $request->branch_id;
        $table_no = $request->table_no;
        $data = Tables::where('tables_id', $tables_id)->update(['table_no' => $table_no]);

        return redirect()->back();
    }
}
    //--------------------------------------------------
