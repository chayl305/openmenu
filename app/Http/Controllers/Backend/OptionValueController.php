<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Language;
use App\Models\Page;
use App\Models\Shop;
use App\Models\OptionValue;

use Input;
use DB;

class OptionValueController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\OptionValue'; // Model
        $this->obj_model = new $this->model; // Obj Model

        $this->model_tr = 'App\Models\OptionValueTr'; // Model
        $this->obj_model_tr = new $this->model_tr; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Option Value'; // Page Title
        $this->a_search = ['name']; // Array Search
        $this->path = '_admin/option_value'; // Url Path
        $this->view_path = 'backend.option_value.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "option_value.".$primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $shop_id = Input::get('shop_id');

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $obj_model->leftjoin('option_value_tr','option_value.'.$primaryKey,'=','option_value_tr.'.$primaryKey)
        // ->leftjoin('shop','option_value.shop_id','=','shop.shop_id')
        // ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
        ->where('option_value_tr.lang',$main_lang);
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 1){
            $data = $data;
        }elseif (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                $data = $data->leftjoin('shop','option_value.shop_id','=','shop.shop_id')
                        ->where('shop.owner_id', session()->get('s_owner_id'));
                if(!empty($shop_id)){
                    $ownerId = Shop::where('shop_id', $shop_id)->first()->owner_id;
                    if (session()->get('s_owner_id') <> $ownerId) {
                        return abort(503);
                    }else{
                        $data = $data->where('option_value.shop_id', $shop_id);
                    }
                }
            }
        }elseif (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {   
                $data = $data->where('option_value.shop_id', session()->get('s_shop_id'));
                if (!empty($shop_id))
                {   
                    $shopId = Shop::where('shop_id', $shop_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }else{
                        $data = $data;
                    }
                }
            }
        }else{
            return abort(503);
        }
        /*------------------------- clost permission -------------------------*/

        if(!empty($shop_id)){
            $data = $data->where('option_value.shop_id',$shop_id);
        }else{

             return abort(503);
        }
        
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere("option_value_tr.".$field,'like','%'.$search.'%');
                }
            });
        }
        $count_data = $data->count();
        $data = $data->groupBy('option_value.'.$primaryKey);
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');
        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
        {   
            if (!empty(Input::get('shop_id'))){
                if (session()->get('s_shop_id') <> Input::get('shop_id')) {
                    return abort(503);
                }
            }
        }
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(!empty(Input::get('shop_id'))){
                $ownerId = Shop::where('shop_id', Input::get('shop_id'))->first()->owner_id;
                if (session()->get('s_owner_id') <> $ownerId) {
                    return abort(503);
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
       // End Language
        $shop_id = input::get('shop_id');
        if(!empty($shop_id)){
            $data = $obj_model->where('shop_id',$shop_id);
        }else{

             return abort(503);
        }

        $data_shop = DB::table('shop')
            ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
            ->select('shop_tr.shop_name','shop.shop_id')
            ->whereNull('shop.deleted_at')
            ->where('shop_tr.lang',$main_lang)
            ->get();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','data_shop'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $str_param = $request->str_param;
        $permission = $obj_fn->permission($this->page_id,'c');

        $input = $request->all(); // Get all post from form
        if(!empty($request->shop_id)){
            $input['shop_id'] = $request->shop_id;
        }
        $input['is_available'] = 1;
        $data = $obj_model->create($input);
        $input[$primaryKey] = $data->$primaryKey;

        foreach ($request->name as $key => $value){
            $input['name'] = $value;
            $input['lang'] = $key;
            $obj_model_tr->create($input);
        }

        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        /*------------------------- open permission -------------------------*/
        $shopId = OptionValue::where('option_value_id', $id)->first()->shop_id;
        if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
        {   
            if(session()->get('s_shop_id') != $shopId){
                return abort(503);
            }
            if (!empty(Input::get('shop_id'))){
                if (session()->get('s_shop_id') <> Input::get('shop_id')) {
                    return abort(503);
                }
            }
        }
        $ownerId = Shop::leftJoin('option_value','shop.shop_id','=','option_value.shop_id')->where('option_value.option_value_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(session()->get('s_owner_id') != $ownerId){
                return abort(503);
            }
            if(!empty(Input::get('shop_id'))){
                $ownerId = Shop::where('shop_id', Input::get('shop_id'))->first()->owner_id;
                if (session()->get('s_owner_id') <> $ownerId) {
                    return abort(503);
                }
            }else{
                return abort(503);
            }
        }
        /*------------------------- clost permission -------------------------*/

        $shop_id = input::get('shop_id');
        if(!empty($shop_id)){
            $data = $obj_model->where('shop_id',$shop_id);
        }else{

             return abort(503);
        }
        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $obj_model->find($id);
        $data_tr = $obj_model_tr->where($primaryKey,$id)->get();

        foreach ($data_tr as $value){
            $name[$value->lang] = $value->name;
        }

        $data_shop = DB::table('shop')
            ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
            ->select('shop_tr.shop_name','shop.shop_id')
            ->whereNull('shop.deleted_at')
            ->where('shop_tr.lang',$main_lang)
            ->get();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','data_shop','name'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $primaryKeyTr = $obj_model_tr->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);
        $available_lang = Language::select('lang')->where('is_available','1')->get();

        foreach ($request->name as $key => $value){
            $dataTr = $obj_model_tr::where($primaryKey,$id)->select($primaryKeyTr)->where('lang',$key);
            $countTr = $dataTr->count();
            $dataTr = $dataTr->first();

            if($countTr > 0){
                $input['name'] = $value;
                $input['lang'] = $key;
                $obj_model_tr->find($dataTr->$primaryKeyTr)->update($input);
            }else{
                $input[$primaryKey] = $id;
                $input['name'] = $value;
                $input['lang'] = $key;
                $obj_model_tr->create($input);
            }

        }

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $primaryKeyTr = $obj_model_tr->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'d');

        $dataTr = $obj_model_tr::where($primaryKey,$id)->select($primaryKeyTr)->get();
        foreach ($dataTr as $value){
            $obj_model_tr::where($primaryKeyTr,$value->$primaryKeyTr)->forceDelete();
        }

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
