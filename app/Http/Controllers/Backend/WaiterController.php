<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Language;
use App\Models\Shop;
use App\Models\Page;
use App\Models\Tables;
use App\Models\Branch;
use App\Models\Waiter;

use Input;
use DB;
use Hash;

class WaiterController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Waiter'; // Model
        $this->obj_model = new $this->model; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Waiter'; // Page Title
        $this->a_search = ['waiter_name']; // Array Search
        $this->path = '_admin/waiter_user'; // Url Path
        $this->view_path = 'backend.waiter.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');
        $primaryKey = $obj_model->primaryKey;

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $shop_id= Input::get('shop_id');

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $obj_model;

        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(!empty($shop_id)){
                    $ownerId = Shop::where('shop_id', $shop_id)->first()->owner_id;
                    if (session()->get('s_owner_id') <> $ownerId) {
                        return abort(503);
                    }
                }else{
                    return abort(503);
                }
            }
        }
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {   
                $data = $data->where('shop_id', session()->get('s_shop_id'));
                if (!empty($shop_id))
                {   
                    $shopId = Shop::where('shop_id', $shop_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        if (!empty($shop_id))
        {
            $data = $data->where('shop_id', $shop_id);
        }else{
            return abort(503);
        }
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }

        $count_data = $data->count();
        $data = $data->groupBy($primaryKey);
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        $data_branch = Branch::all();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','data_branch'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');
        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        $shop_id= Input::get('shop_id');
        $data = $obj_model;
        // End Language
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
        {   
            if (!empty(Input::get('shop_id'))){
                if (session()->get('s_shop_id') <> Input::get('shop_id')) {
                    return abort(503);
                }
            }
        }
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(!empty(Input::get('shop_id'))){
                $ownerId = Shop::where('shop_id', Input::get('shop_id'))->first()->owner_id;
                if (session()->get('s_owner_id') <> $ownerId) {
                    return abort(503);
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        $shop_id = Input::get('shop_id');
        if (!empty($shop_id))
        {
            $data = $obj_model->where('shop_id', $shop_id);
        }else{
            return abort(503);
        }

        $data_branch = DB::table('branch')
            ->leftjoin('branch_tr','branch.branch_id','=','branch_tr.branch_id')
            ->select('branch_tr.branch_name','branch.branch_id')
            ->whereNull('branch.deleted_at')
            ->where('branch.shop_id',$shop_id)
            ->where('branch_tr.lang',$main_lang)
            ->get();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','data_branch'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $branch = '';
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'c');

        foreach ($request->branch as $key => $value) {
            $branch .= ",".$value;
        }
        Input::merge(array('branch' => substr($branch, 1)));
        $str_param = $request->str_param;
        $input = $request->all(); // Get all post from form
        if(!empty(session()->get('s_shop_id'))){
            $input['shop_id'] = session()->get('s_shop_id');
        }
        if(!empty($request->shop_id)){
            $input['shop_id'] = $request->shop_id;
        }
        $input['password'] = Hash::make($request->password);
        $data = $obj_model->create($input);
        $input[$primaryKey] = $data->$primaryKey;
        
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        $shop_id= Input::get('shop_id');
        $data = $obj_model;
        /*------------------------- open permission -------------------------*/
        $shopId = Waiter::where('waiter_id', $id)->first()->shop_id;
        if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
        {   
            if(session()->get('s_shop_id') != $shopId){
                return abort(503);
            }
            if (!empty(Input::get('shop_id'))){
                if (session()->get('s_shop_id') <> Input::get('shop_id')) {
                    return abort(503);
                }
            }
        }
        $ownerId = Shop::leftJoin('waiter','shop.shop_id','=','waiter.shop_id')->where('waiter.waiter_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(session()->get('s_owner_id') != $ownerId){
                return abort(503);
            }
            if(!empty(Input::get('shop_id'))){
                $ownerId = Shop::where('shop_id', Input::get('shop_id'))->first()->owner_id;
                if (session()->get('s_owner_id') <> $ownerId) {
                    return abort(503);
                }
            }else{
                return abort(503);
            }
        }
        /*------------------------- clost permission -------------------------*/
        $shop_id = Input::get('shop_id');
        if (!empty($shop_id))
        {
            $data = $obj_model->where('shop_id', $shop_id);
        }else{
            return abort(503);
        }
        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $data->find($id);

        $data_branch = DB::table('branch')
            ->leftjoin('branch_tr','branch.branch_id','=','branch_tr.branch_id')
            ->select('branch_tr.branch_name','branch.branch_id')
            ->whereNull('branch.deleted_at')
            ->where('branch.shop_id',$shop_id)
            ->where('branch_tr.lang',$main_lang)
            ->get();

        $branch_ar = explode(',',$data->branch);
        foreach ($branch_ar as $value){
            $branch_name[$value] = DB::table('branch_tr')->select('branch_name')->where('lang',$main_lang)->where('branch_id',$value)->first();
        }

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','data_shop','branch_name','data_branch'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $branch = '';
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'u');

        foreach ($request->branch as $key => $value) {
            $branch .= ",".$value;
        }
        Input::merge(array('branch' => substr($branch, 1)));
        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);
        
        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());

        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $primaryKeyTr = $obj_model_tr->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'d');

        $dataTr = $obj_model_tr::where($primaryKey,$id)->select($primaryKeyTr)->get();
        foreach ($dataTr as $value){
            $obj_model_tr::where($primaryKeyTr,$value->$primaryKeyTr)->forceDelete();
        }

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
