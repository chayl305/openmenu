<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use Carbon\Carbon;
use App\Models\Shop;
use App\Models\ShopTr;
use App\Models\Tables;
use App\Models\Language;

use Input;
use DB;
use Hash;

class WaiterTestController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\CallLog'; // Model
        $this->obj_model = new $this->model; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Waiter Test'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/waiter'; // Url Path
        $this->view_path = 'backend.waiter_test.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        return view('backend.waiter.index');
    }

    public function postTableNo(Request $request)
    {
        // return Hash::make('test_s');
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;

        $random_key = $request->random_key;
        $action_id = $request->action_id;
        $tables = Tables::where('random_key', $random_key)
                        ->select('tables_id')->first();
        // $time_call = new Carbon();
        $time_call = date("Y-m-d H:i:sa");
        $input = [
                    "waiter_id" => '',
                    "tables_id" => $tables->tables_id,
                    "action_id" => $action_id,
                    "time_call" => $time_call,
                    "finish_time" => ''
                ];
        $data = $obj_model->create($input);
        $input[$primaryKey] = $data->$primaryKey;
 
        return redirect()->to($this->path);
        // return $tables;
    }
}
