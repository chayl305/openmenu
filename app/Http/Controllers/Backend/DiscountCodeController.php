<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Language;
use App\Models\Owner;
use App\Models\Page;
use App\Models\Shop;
use App\Models\DiscountCode;

use Input;
use Hash;
use DB;

class DiscountCodeController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\DiscountCode'; // Model
        $this->obj_model = new $this->model; // Obj Model


        $this->model_tr = 'App\Models\DiscountCodeTr'; // Model
        $this->obj_model_tr = new $this->model_tr; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Discount Code'; // Page Title
        $this->a_search = ['code_name']; // Array Search
        $this->path = '_admin/discount_code'; // Url Path
        $this->view_path = 'backend.discount_code.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');
        $primaryKey = $obj_model->primaryKey;

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "discount_code.".$primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $owner_id = Input::get('owner_id');


        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;

        $data = $obj_model
        ->leftjoin('discount_code_tr','discount_code.'.$primaryKey,'=','discount_code_tr.'.$primaryKey)
        ->leftjoin('owner','discount_code.owner_id','=','owner.owner_id')
                // ->leftjoin('shop','discount_code.shop','=','shop.shop_id')
                // ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
                // ->where('shop_tr.lang',$main_lang)
        ->select(DB::raw('discount_code.*'),'owner.company_name','discount_code_tr.code_name','discount_code.shop');
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                // $data = $data->where('discount_code.owner_id',session()->get('s_owner_id'));
                if(!empty($owner_id)){
                    if (session()->get('s_owner_id') <> $owner_id) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        if (!empty($owner_id))
        {
            $data = $data->where('discount_code.owner_id', $owner_id);
        }else{
            return abort(503);
        }
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere("discount_code_tr.".$field,'like','%'.$search.'%');
                }
            });
        }

        $count_data = $data->count();
        $data = $data->groupBy('discount_code.'.$primaryKey);
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

// return $data;
        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');
        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $shop_name = "";
        /*------------------------- open permission -------------------------*/
        if(!empty(session()->get('s_owner_id'))){
            // $data = $obj_model->where('owner_id','=', session()->get('s_owner_id') );
            if(!empty(Input::get('owner_id'))){
                if (session()->get('s_owner_id') <> Input::get('owner_id')) {
                    return abort(503);
                }
            }
        }
        /*------------------------- clost permission -------------------------*/
        $owner_id = Input::get('owner_id');
        if (!empty($owner_id))
        {
            $data = $obj_model->where('discount_code.owner_id', $owner_id);
        }else{
            return abort(503);
        }


        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;

        $data_owner = DB::table('owner')
        ->select(DB::raw('owner.*'),'owner.company_name')
        ->whereNull('owner.deleted_at')
        ->get();

        $data_shop = DB::table('shop')
        ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
        ->select('shop_tr.shop_name','shop.shop_id')
        ->whereNull('shop.deleted_at')
        ->where('shop.owner_id', $owner_id)
        ->where('shop_tr.lang',$main_lang)
        ->get();

        $data_menu = DB::table('menu')
        ->leftjoin('menu_tr','menu.menu_id','=','menu_tr.menu_id')
        ->select('menu_tr.menu_name','menu.menu_id')
        ->whereNull('menu.deleted_at')
        ->where('menu_tr.lang',$main_lang)
        ->get();


        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','data_owner','data_shop','shop_name','data_menu'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'c');
        $str_param = $request->str_param;
        $shop = '';
        $menu_id = '';
 
        foreach ($request->shop as $key => $value) {
            $shop .= ",".$value;
        }

        Input::merge(array('shop' => substr($shop, 1)));
         foreach ($request->menu_id as $key => $value) {
            $menu_id .= ",".$value;
        }

        Input::merge(array('menu_id' => substr($menu_id, 1)));

        $input = $request->all(); // Get all post from form
        if(!empty($request->owner_id)){
            $input['owner_id'] = $request->owner_id;
        }
        $input['is_available'] = 1;
        $data = $obj_model->create($input);
        $input[$primaryKey] = $data->$primaryKey;

         // Language
        $available_lang = Language::select('lang')->where('is_available','1')->get();
        // End Language

        foreach ($available_lang as $key => $value){
            $input['code_name'] = $request->code_name[$value->lang];
            $input['lang'] = $value->lang;
            $obj_model_tr->create($input);
        }
        
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;

        $permission = $obj_fn->permission($this->page_id,'u');
        
        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        $shop_name = "";
        /*------------------------- open permission -------------------------*/
        if(!empty(session()->get('s_owner_id'))){
            if(!empty(Input::get('owner_id'))){
                if (session()->get('s_owner_id') <> Input::get('owner_id')) {
                    return abort(503);
                }
            }
        }
        $ownerId = DiscountCode::where('discount_code_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(session()->get('s_owner_id') != $ownerId){
                return abort(503);
            }
        }
        /*------------------------- clost permission -------------------------*/
        $owner_id = input::get('owner_id');
        if (!empty($owner_id))
        {
            $data = $obj_model->where('discount_code.owner_id', $owner_id);
        }else{
            return abort(503);
        }

         // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $obj_model->find($id);
        $data_tr = $obj_model_tr->where($primaryKey,$id)->get();

        foreach ($data_tr as $value){
            $code_name[$value->lang] = $value->code_name;
        }


        $data_owner = DB::table('owner')
        ->select(DB::raw('owner.*'),'owner.company_name')
        ->whereNull('owner.deleted_at')
        ->get();

        $data_shop = DB::table('shop')
        ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
        ->select('shop_tr.shop_name','shop.shop_id')
        ->whereNull('shop.deleted_at')
        ->where('shop.owner_id', $owner_id)
        ->where('shop_tr.lang',$main_lang)
        ->get();
         $data_menu = DB::table('menu')
        ->leftjoin('menu_tr','menu.menu_id','=','menu_tr.menu_id')
        ->select('menu_tr.menu_name','menu.menu_id')
        ->whereNull('menu.deleted_at')
        ->where('menu_tr.lang',$main_lang)
        ->get();


        $shop_ar = explode(',',$data->shop);
        foreach ($shop_ar as $value){
            $shop_name[$value] = DB::table('shop_tr')->select('shop_name')->where('lang',$main_lang)->where('shop_id',$value)->first();
        }
        $menu_ar = explode(',',$data->menu_id);
        foreach ($menu_ar as $value){
            $menu_name[$value] = DB::table('menu_tr')->select('menu_name')->where('lang',$main_lang)->where('menu_id',$value)->first();
        }

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','code_name','data_owner','data_shop','shop_name','data_menu','menu_name'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'u');
        $shop = '';
        $menu_id = '';
        foreach ($request->shop as $key => $value) {
           if(!empty($value)){
            $shop .= ",".$value;
        }
    }

    Input::merge(array('shop' => substr($shop, 1)));
    foreach ($request->menu_id as $key => $value) {
           if(!empty($value)){
            $menu_id .= ",".$value;
        }
    }

    Input::merge(array('menu_id' => substr($menu_id, 1)));
    
        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $available_lang = Language::select('lang')->where('is_available','1')->get();

        foreach ($available_lang as $key => $value){
            $dataTr = $obj_model_tr::where($primaryKey,$id)->select('discount_code_tr_id')->where('lang',$value->lang);
            $countTr = $dataTr->count();
            $dataTr = $dataTr->first();

            if($countTr > 0){
                $input['code_name'] = $request->code_name[$value->lang];
                $input['lang'] = $value->lang;
                $obj_model_tr->find($dataTr->discount_code_tr_id)->update($input);
            }else{
                $input['discount_code_id'] = $id;
                $input['code_name'] = $request->code_name[$value->lang];
                $input['lang'] = $value->lang;
                $obj_model_tr->create($input);
            }
        }
        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;

        $permission = $obj_fn->permission($this->page_id,'d');
        $dataTr = $obj_model_tr::where($primaryKey,$id)->select('discount_code_tr_id')->get();
        foreach ($dataTr as $value){
            $obj_model_tr::where('discount_code_tr_id',$value->discount_code_tr_id)->forceDelete();
        }

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
