<?php
namespace App\Http\Controllers\Backend;
use App\Models\MenuOption;
use App\Models\OptionGroupTr;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Language;
use App\Models\Shop;
use App\Models\Page;
use App\Models\Menu;
use App\Models\MenuTr;
use App\Models\OptionGroup;

use Input;
use DB;

class MenuOptionController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\MenuOption'; // Model
        $this->obj_model = new $this->model; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Menu Option'; // Page Title
        $this->a_search = ['menu_name']; // Array Search
        $this->path = '_admin/menu_option'; // Url Path
        $this->view_path = 'backend.menu_option.'; // View Path
         $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "menu_option.".$primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $option_group_id = input::get('option_group_id');
        $option_value_id = input::get('option_value_id');
        $menu_id = Input::get('menu_id');
        $shop_id = Input::get('shop_id');

//        $data = $obj_model
//                ->leftjoin('menu','menu_option.menu_id','=','menu.menu_id')
//                ->leftjoin('menu_tr','menu.menu_id','=','menu_tr.menu_id')
//                ->leftjoin('option_group','menu_option.option_group_id','=','option_group.option_group_id')
//                ->leftjoin('option_group_tr','option_group.option_group_id','=','option_group_tr.option_group_id')
//                ->leftjoin('option_value','menu_option.option_value_id','=','option_value.option_value_id')
//                ->leftjoin('option_value_tr','option_value.option_value_id','=','option_value_tr.option_value_id')
//                ->select(DB::raw('menu_option.*'),'menu_tr.menu_name','menu_option.price','option_group_tr.name','option_value_tr.name');

        $data = $obj_model->select('menu_option_id','option_group_id','option_value_id','price');
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(!empty($menu_id)){
                    $ownerId = Menu::leftjoin('shop','menu.shop_id','=','shop.shop_id')
                        ->where('menu.menu_id', $menu_id)->first()->owner_id;
                    if(session()->get('s_owner_id') != $ownerId){
                        return abort(503);
                    }
                }
            }
        }
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {
                if(!empty($menu_id)){  
                    $shopId = Menu::where('menu_id', $menu_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        if(!empty($menu_id)){
            $data = $data->where('menu_option.menu_id',$menu_id);
        }else{
            return abort(503);
        }

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere("menu_tr.".$field,'like','%'.$search.'%');
                }
            });
        }

        if(!empty($option_group_id)){
            $data = $data->where('option_group_id',$option_group_id);
        }

        $count_data = $data->count();
//        $data = $data->groupBy('menu_option.option_group_id');
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;

        $data_menu = MenuTr::where('menu_id',$menu_id)->where('lang',$main_lang)->select('menu_name')->first();
        $data_option_group = MenuOption::leftjoin('option_group','option_group.option_group_id','=','menu_option.option_group_id')->leftjoin('option_group_tr','option_group_tr.option_group_id','=','option_group.option_group_id')->whereNull('option_group.deleted_at')->where('option_group_tr.lang',$main_lang)->where('option_group.shop_id',$shop_id)->where('menu_option.menu_id',$menu_id)->select('option_group_tr.name','option_group.option_group_id')->groupBy('menu_option.option_group_id')->get();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','data_menu','data_option_group'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $page_title = $this->page_title;
          $permission = $obj_fn->permission($this->page_id,'c');

        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        $menu_id = Input::get('menu_id');
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(!empty($menu_id)){
                    $ownerId = Menu::leftjoin('shop','menu.shop_id','=','shop.shop_id')
                        ->where('menu.menu_id', $menu_id)->first()->owner_id;
                    if(session()->get('s_owner_id') != $ownerId){
                        return abort(503);
                    }
                }
            }
        }elseif (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {
                if(!empty($menu_id)){  
                    $shopId = Menu::where('menu_id', $menu_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/
        if(!empty($menu_id)){
            $data = $obj_model->where('menu_option.menu_id',$menu_id);
        }else{

            return abort(503);
        }
        // End Language
        $data_menu = DB::table('menu')
                    ->leftjoin('menu_tr','menu.menu_id','=','menu_tr.menu_id')
                    ->select('menu_tr.menu_name','menu.menu_id')
                    ->whereNull('menu.deleted_at')
                    ->where('menu.menu_id', $menu_id)
                    ->where('menu_tr.lang',$main_lang)
                    ->get();

        $shopId = Menu::where('menu_id',$menu_id)->first()->shop_id;
        $data_option_group = DB::table('option_group')
                    ->leftjoin('option_group_tr','option_group.option_group_id','=','option_group_tr.option_group_id')
                    ->leftjoin('shop','option_group.shop_id','=','shop.shop_id')
                    ->select('option_group_tr.name','option_group.option_group_id')
                    ->whereNull('option_group.deleted_at')
                    ->where('option_group.shop_id', $shopId)
                    ->where('option_group_tr.lang',$main_lang)
                    ->get();

        $data_option_value = DB::table('option_value')
                    ->leftjoin('option_value_tr','option_value.option_value_id','=','option_value_tr.option_value_id')
                    ->select('option_value_tr.name','option_value.option_value_id')
                    ->whereNull('option_value.deleted_at')
                    ->where('option_value.shop_id', $shopId)
                    ->where('option_value_tr.lang',$main_lang)
                    ->get();
        // return $data_menu;
        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','data_option_group','data_option_value','data_menu'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $str_param = $request->str_param;
         $permission = $obj_fn->permission($this->page_id,'c');

        $input = $request->all(); 
          // Get all post from form
        if(!empty($request->menu_id)){
            $input['menu_id'] = $request->menu_id;
        }
        $data = $obj_model->create($input);
        $input[$primaryKey] = $data->$primaryKey;

        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $page_title = $this->page_title;
        $permission = $obj_fn->permission($this->page_id,'u');
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;

        $menu_id = input::get('menu_id');
        /*------------------------- open permission -------------------------*/

        $ownerId1 = Shop::leftJoin('menu','shop.shop_id','=','menu.shop_id')
                ->leftJoin('menu_option','menu.menu_id','=','menu_option.menu_id')
                ->where('menu_option_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(session()->get('s_owner_id') != $ownerId1){
                    return abort(503);
                }
                if(!empty($menu_id)){
                    $ownerId = Menu::leftjoin('shop','menu.shop_id','=','shop.shop_id')
                        ->where('menu.menu_id', $menu_id)->first()->owner_id;
                    if(session()->get('s_owner_id') != $ownerId){
                        return abort(503);
                    }
                }
            }
        }
        $shopId1 = Menu::leftJoin('menu_option','menu.menu_id','=','menu_option.menu_id')
                ->where('menu_option_id', $id)->first()->shop_id;
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {
                if(session()->get('s_shop_id') != $shopId1){
                    return abort(503);
                }
                if(!empty($menu_id)){  
                    $shopId = Menu::where('menu_id', $menu_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/
        if(!empty($menu_id)){
            $data = $obj_model->where('menu_option.menu_id',$menu_id);
        }else{
            return abort(503);
        }
        $data = $obj_model->find($id);
   
        $data_menu = DB::table('menu')
                    ->leftjoin('menu_tr','menu.menu_id','=','menu_tr.menu_id')
                    ->select('menu_tr.menu_name','menu.menu_id')
                    ->whereNull('menu.deleted_at')
                    ->where('menu.menu_id', $menu_id)
                    ->where('menu_tr.lang',$main_lang)
                    ->get();
                    
        $shopId = Menu::where('menu_id',$menu_id)->first()->shop_id;
        $data_option_group = DB::table('option_group')
                    ->leftjoin('option_group_tr','option_group.option_group_id','=','option_group_tr.option_group_id')
                    ->leftjoin('shop','option_group.shop_id','=','shop.shop_id')
                    ->select('option_group_tr.name','option_group.option_group_id')
                    ->whereNull('option_group.deleted_at')
                    ->where('option_group.shop_id', $shopId)
                    ->where('option_group_tr.lang',$main_lang)
                    ->get();

        $data_option_value = DB::table('option_value')
                    ->leftjoin('option_value_tr','option_value.option_value_id','=','option_value_tr.option_value_id')
                    ->select('option_value_tr.name','option_value.option_value_id')
                    ->whereNull('option_value.deleted_at')
                    ->where('option_value.shop_id', $shopId)
                    ->where('option_value_tr.lang',$main_lang)
                    ->get();


        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','data_option_group','data_option_value','menu_name','price','data_menu'));
    }
      // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
         $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        $obj_fn = $this->obj_fn;
        session()->put('ref_url',url()->previous());
        $obj_model = $this->obj_model;
         $permission = $obj_fn->permission($this->page_id,'d');
        $obj_model->find($id)->delete();
        return redirect()->to(session()->get('ref_url'));
    }
}
