<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Language;
use App\Models\Page;
use App\Models\Shop;
use App\Models\Category;

use Input;
use DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Category'; // Model
        $this->obj_model = new $this->model; // Obj Model

        $this->model_tr = 'App\Models\CategoryTr'; // Model
        $this->obj_model_tr = new $this->model_tr; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Category'; // Page Title
        $this->a_search = ['category_name']; // Array Search
        $this->path = '_admin/category'; // Url Path
        $this->view_path = 'backend.category.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'r');
        
        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "category.".$primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $shop_id = Input::get('shop_id');

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $obj_model->leftjoin('category_tr','category.'.$primaryKey,'=','category_tr.'.$primaryKey)
                ->where('category_tr.lang',$main_lang);

        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                $data = $data->leftjoin('shop','category.shop_id','=','shop.shop_id')
                        ->where('shop.owner_id', session()->get('s_owner_id'));
                if(!empty($shop_id)){
                    $ownerId = Shop::where('shop_id', $shop_id)->first()->owner_id;
                    if (session()->get('s_owner_id') <> $ownerId) {
                        return abort(503);
                    }
                }
            }
        }
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {   
                $data = $data->where('category.shop_id', session()->get('s_shop_id'));
                if (!empty($shop_id))
                {   
                    $shopId = Shop::where('shop_id', $shop_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        if (!empty($shop_id))
        {
            $data = $data->where('category.shop_id', $shop_id);
        }else{
            return abort(503);
        }
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere("category_tr.".$field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->groupBy('category.'.$primaryKey);
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');
        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $data = $obj_model;
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
        {   
            if (!empty(Input::get('shop_id'))){
                if (session()->get('s_shop_id') <> Input::get('shop_id')) {
                    return abort(503);
                }
            }
        }
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(!empty(Input::get('shop_id'))){
                $ownerId = Shop::where('shop_id', Input::get('shop_id'))->first()->owner_id;
                if (session()->get('s_owner_id') <> $ownerId) {
                    return abort(503);
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        $shop_id = Input::get('shop_id');
        if (!empty($shop_id))
        {
            $data = $obj_model->where('category.shop_id', $shop_id);
        }else{
            return abort(503);
        }

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
         $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $str_param = $request->str_param;
        $permission = $obj_fn->permission($this->page_id,'c');

        $input = $request->all(); // Get all post from form
        if(!empty($request->shop_id)){
            $input['shop_id'] = $request->shop_id;
        }
        $input['is_available'] = 1;
        $data = $obj_model->create($input);
        $input[$primaryKey] = $data->$primaryKey;

        // Language
        $available_lang = Language::select('lang')->where('is_available','1')->get();
        // End Language

        foreach ($available_lang as $key => $value){
            $input['category_name'] = $request->category_name[$value->lang];
            $input['lang'] = $value->lang;
            $obj_model_tr->create($input);
        }
        if (Input::hasFile('img_name')) { // เพิ่มตรงนี้
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/category');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;

            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   //
            $obj_fn->image_resize($photo, $destinationPath,100, $filename);   // resize image

            $data = $obj_model::find($data->category_id);
            $data->img_name = $filename;
            $data->save();

        }

        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model;
        /*------------------------- open permission -------------------------*/
        $shopId = Category::where('category_id', $id)->first()->shop_id;
        if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
        {   
            if(session()->get('s_shop_id') != $shopId){
                return abort(503);
            }
            if (!empty(Input::get('shop_id'))){
                if (session()->get('s_shop_id') <> Input::get('shop_id')) {
                    return abort(503);
                }
            }
        }
        $ownerId = Shop::leftJoin('category','shop.shop_id','=','category.shop_id')->where('category.category_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(session()->get('s_owner_id') != $ownerId){
                return abort(503);
            }
            if(!empty(Input::get('shop_id'))){
                $ownerId = Shop::where('shop_id', Input::get('shop_id'))->first()->owner_id;
                if (session()->get('s_owner_id') <> $ownerId) {
                    return abort(503);
                }
            }else{
                return abort(503);
            }
        }
        /*------------------------- clost permission -------------------------*/
        $shop_id = Input::get('shop_id');
        if (!empty($shop_id))
        {
            $data = $obj_model->where('category.shop_id', $shop_id);
        }else{
            return abort(503);
        }

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $data->find($id);
        $data_tr = $obj_model_tr->where($primaryKey,$id)->get();

        foreach ($data_tr as $value){
            $category_name[$value->lang] = $value->category_name;
        }

        $data_shop = DB::table('shop')
            ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
            ->select('shop_tr.shop_name','shop.shop_id')
            ->whereNull('shop.deleted_at')
            ->where('shop.shop_id',$shop_id)
            ->where('shop_tr.lang',$main_lang)
            ->get();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','data_shop','category_name'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
         $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        // Language
        $available_lang = Language::select('lang')->where('is_available','1')->get();
        // End Language

        foreach ($available_lang as $key => $value){
            $dataTr = $obj_model_tr::where($primaryKey,$id)->select('category_tr_id')->where('lang',$value->lang);
            $countTr = $dataTr->count();
            $dataTr = $dataTr->first();

            if($countTr > 0){
                $input['category_name'] = $request->category_name[$value->lang];
                $input['lang'] = $value->lang;
                $obj_model_tr->find($dataTr->category_tr_id)->update($input);
            }else{
                $input['category_id'] = $id;
                $input['category_name'] = $request->category_name[$value->lang];
                $input['lang'] = $value->lang;
                $obj_model_tr->create($input);
            }

        }
        $data = $obj_model->find($id);
        if (Input::hasFile('img_name')) { // test P.
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/category');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $obj_fn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $obj_model::find($id);
            $data->img_name = $filename;
            $data->save();
        }
        else if ($request->img_del == 'y'){
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/category');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage

            $data->img_name = '';
            $data->save();
        }

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'d');

        $dataTr = $obj_model_tr::where($primaryKey,$id)->select('category_tr_id')->get();
        foreach ($dataTr as $value){
            $obj_model_tr::where('category_tr_id',$value->category_tr_id)->forceDelete();
        }

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
