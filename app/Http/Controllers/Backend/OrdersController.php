<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Orders;
use App\Models\Shop;
use App\Models\Language;
use App\Models\Page;
use App\Models\Owner;

use Input;
use Hash;
use DB;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Orders'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Order'; // Page Title
        $this->a_search = ['orders_no']; // Array Search
        $this->path = '_admin/orders'; // Url Path
        $this->view_path = 'backend.orders.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $shop_id = Input::get('shop_id');
        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language
        $data = $obj_model;
        // $data = $data::leftJoin('shop', 'orders.shop_id', '=', 'shop.shop_id');
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 1){
            $data = $data;
        }elseif (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                $data = $data->leftJoin('shop', 'orders.shop_id', '=', 'shop.shop_id')
                            ->where('shop.owner_id', session()->get('s_owner_id'))
                            ->select(DB::raw('orders.*'));
            }
        }elseif (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {   
                $data = $data->where('shop_id', session()->get('s_shop_id'));
            }
        }else{
            return abort(503);
        }
        /*------------------------- clost permission -------------------------*/

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $from_date = Input::get('from_date');
        if(!empty($from_date)){
            $data = $data->where('orders.created_at','>=',$from_date);
        }

        $to_date = Input::get('to_date');
        if(!empty($to_date)){
            $data = $data->where('orders.created_at','<=',$to_date);
        }
        if(!empty($shop_id)){
            $data = $data->where('orders.shop_id',$shop_id);
        }
        $count_data = $data->count();
        $data = $data->groupBy('orders.'.$primaryKey);
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);
        $site = Shop::leftJoin('shop_tr','shop.shop_id','=','shop_tr.shop_id');
        if(!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0){
            $site = $site->where('shop.owner_id',session()->get('s_owner_id') );
        }
        $site = $site->groupBy('shop.shop_id')->get();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','site'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {

    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        
    }
}
