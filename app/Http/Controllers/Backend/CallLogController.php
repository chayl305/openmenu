<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Tables;
use App\Models\Branch;
use App\Models\BranchTr;
use App\Models\Language;
use Carbon\Carbon;


use Input;
use Hash;
use DB;

class CallLogController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\CallLog'; // Model
        $this->obj_model = new $this->model; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Call Log'; // Page Title
        $this->a_search = ['branch_name']; // Array Search
        $this->path = '_admin/call_log'; // Url Path
        $this->view_path = 'backend.call_log.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "call_log.".$obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'asc';

        $search = Input::get('search');

        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;

        // End Language
        $data = $obj_model->join('tables','call_log.tables_id','=','tables.tables_id')
                        ->join('branch','tables.branch_id','=','branch.branch_id')
                        ->join('branch_tr','branch.branch_id','=','branch_tr.branch_id')
                        ->select(DB::raw('call_log.*'),'tables.table_no as table_no','branch_tr.branch_name')
                        ->where('branch_tr.lang',$main_lang)
                        ->where('finish_time','=','00:00:00');
                            
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);
        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
      
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
       
    }
    // ------------------------------------ Record Update Data
    public function postFinishTime(Request $request)
    {
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        // $finish_time = new Carbon();

        $finish_time = date("Y-m-d H:i:sa");
        $waiter = session()->get('s_admin_username');

        DB::table('call_log')->where('call_log_id',$request->finish)->update(['finish_time' => $finish_time ]);
        return redirect()->to($this->path);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
       
    }
}
