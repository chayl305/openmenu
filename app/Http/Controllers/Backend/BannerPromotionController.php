<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Language;
use App\Models\Owner;
use App\Models\Page;
use App\Models\Shop;
use App\Models\BannerPromotion;

use Input;
use Hash;
use DB;

class BannerPromotionController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\BannerPromotion'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Banner Promotion'; // Page Title
        $this->a_search = ['owner.company_name']; // Array Search
        $this->path = '_admin/banner_promotion'; // Url Path
        $this->view_path = 'backend.banner_promotion.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;

        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = 'banner_promotion.'.$primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $owner_id = Input::get('owner_id');
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;

        $data = $obj_model->leftjoin('owner','banner_promotion.owner_id','=','owner.owner_id')->select(DB::raw('banner_promotion.*'),'owner.company_name');
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                // $data = $data->where('banner_promotion.owner_id',session()->get('s_owner_id'));
                if(!empty($owner_id)){
                    if (session()->get('s_owner_id') <> $owner_id) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        if(!empty($owner_id)){
            $data = $data->where('banner_promotion.owner_id',$owner_id);
        }else{
            return abort(503);
        }

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field,'like','%'.$search.'%');
                }
            });
        }

        $count_data = $data->count();
//        $data = $data->groupBy('banner_promotion.'.$primaryKey);
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','main_lang'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');
        $shop_name = "";

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        /*------------------------- open permission -------------------------*/
        if(!empty(session()->get('s_owner_id'))){
            // $data = $obj_model->where('owner_id','=', session()->get('s_owner_id') );
            if(!empty(Input::get('owner_id'))){
                if (session()->get('s_owner_id') <> Input::get('owner_id')) {
                    return abort(503);
                }
            }
        }
        /*------------------------- clost permission -------------------------*/
        if(!empty(Input::get('owner_id'))){
            $data = $obj_model->where('banner_promotion.owner_id',Input::get('owner_id'));
        }else{
            return abort(503);
        }
        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language
        $data_owner = DB::table('owner')
        ->select(DB::raw('owner.*'),'owner.company_name')
        ->whereNull('owner.deleted_at')
        ->get();

        $data_shop = DB::table('shop')
            ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
            ->select('shop_tr.shop_name','shop.shop_id')
            ->whereNull('shop.deleted_at') 
            ->where('shop_tr.lang',$main_lang)
            ->where('shop.owner_id',Input::get('owner_id'))
            ->get();


        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','roles','data_shop','data_owner','shop_name'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $shop = ''; 
        $obj_model = $this->obj_model;
        $str_param = $request->str_param;
        $permission = $obj_fn->permission($this->page_id,'c');


        foreach ($request->shop as $key => $value) {
            $shop .= ",".$value;
        }

        Input::merge(array('shop' => substr($shop, 1)));

        $input = $request->all(); // Get all post from form
        $input['owner_id'] = Input::get('owner_id');
        $input['is_available'] = 1;
        $data = $obj_model->create($input);

        if (Input::hasFile('img_name')) { // เพิ่มตรงนี้
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/banner_promotion');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;

            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   //
            $obj_fn->image_resize($photo, $destinationPath,100, $filename);   // resize image

            $data = $obj_model::find($data->banner_promotion_id);
            $data->img_name = $filename;
            $data->save();

        }

        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        /*------------------------- open permission -------------------------*/
        if(!empty(session()->get('s_owner_id'))){
            if(!empty(Input::get('owner_id'))){
                if (session()->get('s_owner_id') <> Input::get('owner_id')) {
                    return abort(503);
                }
            }
        }
        $ownerId = BannerPromotion::where('banner_promotion_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(session()->get('s_owner_id') != $ownerId){
                return abort(503);
            }
        }
        /*------------------------- clost permission -------------------------*/
        if(!empty(Input::get('owner_id'))){
            $data = $obj_model->where('banner_promotion.owner_id',Input::get('owner_id'));
        }else{
            return abort(503);
        }

        $data = $obj_model->find($id);

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language


        $data_owner = DB::table('owner')
        ->select(DB::raw('owner.*'),'owner.company_name')
        ->whereNull('owner.deleted_at')
        ->get();

        $data_shop = DB::table('shop')
            ->leftjoin('shop_tr','shop.shop_id','=','shop_tr.shop_id')
            ->select('shop_tr.shop_name','shop.shop_id')
            ->whereNull('shop.deleted_at')
            ->where('shop_tr.lang',$main_lang)
            ->where('shop.owner_id',input::get('owner_id'))
            ->get();

        $shop_ar = explode(',',$data->shop);
        foreach ($shop_ar as $value){
            $shop_name[$value] = DB::table('shop_tr')->select('shop_name')->where('lang',$main_lang)->where('shop_id',$value)->first();
        }

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','roles','data_shop','data_owner','shop_name'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $shop='';
        $permission = $obj_fn->permission($this->page_id,'u');

        foreach ($request->shop as $key => $value) {
            if(!empty($value)){
                $shop .= ",".$value;
            }
        }

        Input::merge(array('shop' => substr($shop, 1)));


        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $data = $obj_model->find($id);
        if (Input::hasFile('img_name')) { // test P.
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/banner_promotion');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $obj_fn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $obj_model::find($id);
            $data->img_name = $filename;
            $data->save();
        }
        else if ($request->img_del == 'y'){
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/banner_promotion');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage

            $data->img_name = '';
            $data->save();
        }

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'d');
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
