<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Kitchen;

use Input;
use Hash;

class CheckKitchenController extends Controller
{
    public function checkUser(Request $request)
    {
        $data = Kitchen::where('username',$request->username)->first();
        if(empty($data))
        {
            return response()->json(['status'=>'OK', 'message' => 'Available']);
        }
        else
        {
            return response()->json(['status'=>'ERROR', 'message' => 'Not Available']);
        }
    }

}
