<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Page;
use App\Models\Permission;
use App\Models\AdminRole;

use DB;
use Input;
use Hash;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Permission'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Permission'; // Page Title
        $this->a_search = ['page_name']; // Array Search
        $this->path = '_admin/permission'; // Url Path
        $this->view_path = 'backend.permission.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');

        $admin_role_id = Input::get('admin_role_id');

        //add some page permisson
        $data_page = Page::get();
        foreach ($data_page as $key => $field) {
            $count_permission = Permission::where('page_id','=',$field->page_id)
                                            ->where('admin_role_id','=',$admin_role_id)
                                            ->count();
            if ($count_permission == 0 ) {
                DB::table('permission')->insert(
                    ['page_id' => $field->page_id, 'admin_role_id' => $admin_role_id, 'c' => 0, 'r' => 0, 'u' => 0, 'd' => 0]
                );
            }
        }

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');
        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $data = $obj_model->join('page','page.page_id','=','permission.page_id')
                        ->where('admin_role_id','=',$admin_role_id)
                        ->whereNull('page.deleted_at');

        $search = Input::get('search');
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        $admin_role = AdminRole::where('admin_role_id','=',$admin_role_id)
                    ->select('role_name')
                    ->first();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','admin_role'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";


        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        $input = $request->all(); // Get all post from form
        $input['password'] = Hash::make($request->password);

        $data = $obj_model->create($input);

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);


        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $input['password'] = Hash::make($request->password);

        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());

        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'d');

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
