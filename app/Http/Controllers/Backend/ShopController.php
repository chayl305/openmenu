<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Page;
use App\Models\Owner;
use App\Models\Shop;
use App\Models\ShopTr;
use App\Models\Language;
use App\Models\Branch;
use App\Models\BranchTr;

use Input;
use DB;
use Hash;

class ShopController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Shop'; // Model
        $this->obj_model = new $this->model; // Obj Model

        $this->model_tr = 'App\Models\ShopTr'; // Model
        $this->obj_model_tr = new $this->model_tr; // Obj Model

        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Site'; // Page Title
        $this->a_search = ['shop_name']; // Array Search
        $this->path = '_admin/shop'; // Url Path
        $this->view_path = 'backend.shop.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'r');
        $obj_model_tr = $this->obj_model_tr;

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "shop.".$primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $owner_id = Input::get('owner_id');

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data = $obj_model->leftjoin('shop_tr','shop.'.$primaryKey,'=','shop_tr.'.$primaryKey)
                ->leftjoin('business_type','shop.business_type_id','=','business_type.business_type_id')
                ->leftjoin('business_type_tr','business_type.business_type_id','=','business_type_tr.business_type_id')
                ->where('shop_tr.lang',$main_lang);

        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                $data = $data->where('shop.owner_id','=', session()->get('s_owner_id') );
                if(!empty($owner_id)){
                    if (session()->get('s_owner_id') <> $owner_id) {
                        return abort(503);
                    }
                }
            }
        }
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {
                return redirect()->to($this->path.'/'.session()->get('s_shop_id').'/edit?1');
                // $data = $data->where('shop.shop_id','=', session()->get('s_shop_id') );
            }
        }
        /*------------------------- clost permission -------------------------*/

        if (!empty($owner_id))
        {
            $data = $data->where('shop.owner_id', $owner_id);
        }
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere("shop_tr.".$field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->groupBy('shop.'.$primaryKey);
        $data = $data->orderBy('shop.shop_id',$sort_by);
        $data = $data->paginate($per_page);

        $owner = Owner::all();
        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','owner'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $owner_id = Input::get('owner_id');
        if(!empty(session()->get('s_owner_id'))){
            $data = $obj_model->where('owner_id','=', session()->get('s_owner_id') );
            if(!empty($owner_id)){
                if (session()->get('s_owner_id') <> $owner_id) {
                    return abort(503);
                }else{
                    $data = $obj_model->where('shop.owner_id','=', $owner_id);
                }
            }
        }
        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data_business = DB::table('business_type')
            ->leftjoin('business_type_tr','business_type.business_type_id','=','business_type_tr.business_type_id')
            ->select('business_type_tr.name','business_type.business_type_id')
            ->whereNull('business_type.deleted_at')
            ->where('business_type_tr.lang',$main_lang)
            ->get();
        $owner = Owner::all();
        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','data_business','owner'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'c');
        $str_param = $request->str_param;
        $input = $request->all(); // Get all post from form
        if(!empty(session()->get('s_owner_id'))){
            $input['owner_id'] = session()->get('s_owner_id');
        }
        if(!empty($request->owner_id)){
            $input['owner_id'] = $request->owner_id;
        }
        $input['is_available'] = 1;
        $data = $obj_model->create($input);

        $input[$primaryKey] = $data->$primaryKey;

        // // Language
        $available_lang = Language::select('lang')->where('is_available','1')->get();
        // End Language
        // Add Name Shop
        foreach ($available_lang as $key => $value){
            $input['shop_name'] = $request->shop_name[$value->lang];
            $input['lang'] = $value->lang;
            $obj_model_tr->create($input);
        }
 
        $number_of_table = $request->number_of_table;
        
        $zone = $obj_fn->gen_branch($input[$primaryKey], $request->branch_name); // Add Zone   
        $waiter = $obj_fn->gen_waiter($input[$primaryKey]); // Add Waiter User
        $kitchen = $obj_fn->gen_kitchen($input[$primaryKey]); // Add Kitchen User
        $tables = $obj_fn->gen_table($input[$primaryKey], $number_of_table); // Add Table
        $password = Hash::make($request->password);
        $user_shop = $obj_fn->gen_user_shop($input[$primaryKey], $request->username,$password); // Add Waiter User

        // Add img
        if (Input::hasFile('img_logo')) { // เพิ่มตรงนี้
            $photo = $request->file('img_logo');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/shop');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;

            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   //
            $obj_fn->image_resize($photo, $destinationPath,100, $filename);   // resize image

            $data = $obj_model::find($data->shop_id);
            $data->img_logo = $filename;
            $data->save();

        }
        
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        /*------------------------- open permission -------------------------*/
        if(!empty(session()->get('s_owner_id'))){
            if(!empty(Input::get('owner_id'))){
                if (session()->get('s_owner_id') <> Input::get('owner_id')) {
                    return abort(503);
                }
            }
        }
        $ownerId = Shop::where('shop_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
        {
            if(session()->get('s_owner_id') != $ownerId){
                return abort(503);
            }
        }
        if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0)
        {
            if($id != session()->get('s_shop_id')){
                return abort(503);
            }
        }
        /*------------------------- clost permission -------------------------*/
        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;

        $data = $obj_model->find($id);
        $data_tr = $obj_model_tr->where($primaryKey,$id)->get();
        

        foreach ($data_tr as $value){
            $shop_name[$value->lang] = $value->shop_name;
        }

        $data_business = DB::table('business_type')
            ->leftjoin('business_type_tr','business_type.business_type_id','=','business_type_tr.business_type_id')
            ->select('business_type_tr.name','business_type.business_type_id')
            ->whereNull('business_type.deleted_at')
            ->where('business_type_tr.lang',$main_lang)
            ->get();
        $owner = Owner::all();
        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','data_business','shop_name','owner'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        // Language
        $available_lang = Language::select('lang')->where('is_available','1')->get();
        // End Language

        foreach ($available_lang as $key => $value){
            $dataTr = $obj_model_tr::where($primaryKey,$id)->select('shop_tr_id')->where('lang',$value->lang);
            $countTr = $dataTr->count();
            $dataTr = $dataTr->first();

            if($countTr > 0){
                $input['shop_name'] = $request->shop_name[$value->lang];
                $input['lang'] = $value->lang;
                $obj_model_tr->find($dataTr->shop_tr_id)->update($input);
            }else{
                $input['shop_id'] = $id;
                $input['shop_name'] = $request->shop_name[$value->lang];
                $input['lang'] = $value->lang;
                $obj_model_tr->create($input);
            }

        }
        $data = $obj_model->find($id);
        if (Input::hasFile('img_logo')) { // test P.
            $photo = $request->file('img_logo');       // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);// set new name
            $old_name = $data->img_logo;           // get old name
            $path = public_path('uploads/shop');           // set path
            $obj_fn->del_storage($path,$old_name);      // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);  // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;     // set filename
            $destinationPath = $path;
            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $obj_fn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $obj_model::find($id);
            $data->img_logo = $filename;
            $data->save();
        }
        else if ($request->img_del == 'y'){
            $old_name = $data->img_logo;                  // get old name
            $path = public_path('uploads/shop');         // set path
            $obj_fn->del_storage($path,$old_name);  // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage

            $data->img_logo = '';
            $data->save();
        }

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $obj_model_tr = $this->obj_model_tr;
        $permission = $obj_fn->permission($this->page_id,'d');

        $dataTr = $obj_model_tr::where($primaryKey,$id)->select('shop_tr_id')->get();
        foreach ($dataTr as $value){
            $obj_model_tr::where('shop_tr_id',$value->shop_tr_id)->forceDelete();
        }

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
