<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Language;
use App\Models\Page;
use App\Models\Branch;
use App\Models\Shop;

use Input;
use Hash;
use DB;
use DNS1D;
use DNS2D;

class TablesController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Tables'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Tables'; // Page Title
        $this->a_search = ['table_no']; // Array Search
        $this->path = '_admin/tables'; // Url Path
        $this->view_path = 'backend.tables.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        // return DNS2D::getBarcodeHTML("4445645656", "QRCODE");
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'r');
        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'asc';

        $search = Input::get('search');
        $branch_id = Input::get('branch_id');

        $data = $obj_model;
        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 1){
            $data = $data;
        }elseif (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(!empty($branch_id)){
                    $ownerId = Branch::leftjoin('shop','branch.shop_id','=','shop.shop_id')
                        ->where('branch.branch_id', $branch_id)->first()->owner_id;
                    if(session()->get('s_owner_id') != $ownerId){
                        return abort(503);
                    }
                }
            }
        }elseif (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {
                if(!empty($branch_id)){  
                    $shopId = Branch::where('branch_id', $branch_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }else{
            return abort(503);
        }
        /*------------------------- clost permission -------------------------*/

        if(!empty($branch_id)){
            $data = $data->where('branch_id',$branch_id);
        }else{
            return abort(503);
        }

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field,'like','%'.$search.'%');
                }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        if(!empty(Input::get('mode'))){

            $shop = DB::table('branch')
            ->leftJoin('shop','branch.shop_id','=', 'shop.shop_id')
            ->leftJoin('shop_tr','shop.shop_id','=', 'shop_tr.shop_id')
            ->where('branch.branch_id', $branch_id )
            ->where('shop_tr.lang', 'th')->first();

            return view($this->view_path.'/export_report',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','shop','qr_code'));
        }

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $branch_id = Input::get('branch_id');

        /*------------------------- open permission -------------------------*/
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(!empty($branch_id)){
                    $ownerId = Branch::leftjoin('shop','branch.shop_id','=','shop.shop_id')
                        ->where('branch.branch_id', $branch_id)->first()->owner_id;
                    if(session()->get('s_owner_id') != $ownerId){
                        return abort(503);
                    }
                }
            }
        }
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {
                if(!empty($branch_id)){  
                    $shopId = Branch::where('branch_id', $branch_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        if(!empty($branch_id)){
            $data = $obj_model->where('branch_id',$branch_id);
        }else{
            return abort(503);
        }

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','roles'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $str_param = $request->str_param;
        $permission = $obj_fn->permission($this->page_id,'c');

        $qty = Input::get('qty');
        $input = $request->all(); // Get all post from form
        $branch_id = $request->branch_id;
        $chect_table = DB::table('tables')
                            ->where('branch_id', $branch_id)
                            ->select('table_no')->orderBy('tables_id','desc');
        $count_table = $chect_table->count();
        $chect_table = $chect_table->get();
        $j=1;
        if(!empty($count_table) && $count_table != 0){
            $j = $count_table+1;
            $qty = $count_table+$qty;
        }
        for($i=$j; $i<=$qty; $i++){
            $input = [
                    "branch_id" =>  $branch_id,
                    "random_key" => $obj_fn->gen_random(9,1),
                    "table_no" => $i
            ];
            $data = $obj_model->create($input);
        }

        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        $branch_id = Input::get('branch_id');

        /*------------------------- open permission -------------------------*/
        $ownerId1 = Shop::leftJoin('branch','shop.shop_id','=','branch.shop_id')->leftJoin('tables','branch.branch_id','=','tables.branch_id')->where('tables.tables_id', $id)->first()->owner_id;
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 2){
            if (!empty(session()->get('s_owner_id')) && session()->get('s_owner_id') != 0)
            {
                if(session()->get('s_owner_id') != $ownerId1){
                    return abort(503);
                }
                if(!empty($branch_id)){
                    $ownerId = Branch::leftjoin('shop','branch.shop_id','=','shop.shop_id')
                        ->where('branch.branch_id', $branch_id)->first()->owner_id;
                    if(session()->get('s_owner_id') != $ownerId){
                        return abort(503);
                    }
                }
            }
        }
        $shopId1 = Branch::leftJoin('tables','branch.branch_id','=','tables.branch_id')->where('tables.tables_id', $id)->first()->shop_id;
        if (!empty(session()->get('s_admin_role_id')) && session()->get('s_admin_role_id') == 3){
            if (!empty(session()->get('s_shop_id')) && session()->get('s_shop_id') != 0) 
            {
                if(session()->get('s_shop_id') != $shopId1){
                    return abort(503);
                }
                if(!empty($branch_id)){  
                    $shopId = Branch::where('branch_id', $branch_id)->first()->shop_id;
                    if (session()->get('s_shop_id') <> $shopId) {
                        return abort(503);
                    }
                }
            }
        }
        /*------------------------- clost permission -------------------------*/

        if(!empty($branch_id)){
            $data = $obj_model->where('branch_id',$branch_id);
        }else{
            return abort(503);
        }

        $data = $obj_model->find($id);

        // Language
        $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
        $main_lang = $main_lang->lang;
        // End Language

        $data_branch = DB::table('branch')
            ->leftjoin('branch_tr','branch.branch_id','=','branch_tr.branch_id')
            ->select('branch_tr.branch_name','branch.branch_id')
            ->whereNull('branch.deleted_at')
            ->where('branch_tr.lang',$main_lang)
            ->get();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','roles','data_branch'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');
        $branch_id = Input::get('branch_id');
        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'d');
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }

    public function postDeleteTable(Request $request)
    {   
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        if(!empty($request->table_chk)){
            foreach ($request->table_chk as $tables_id) {
                $data = $obj_model::where('tables_id', $tables_id)->forceDelete();
            }
        } return redirect()->back();
    }
}
