<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;
    public $table = 'page';
    public $primaryKey = 'page_id';
    public $fillable = ['page_name'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
