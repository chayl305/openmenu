<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class DiscountCode extends Model
{
  use SoftDeletes;
  public $table = 'discount_code';
  public $primaryKey = 'discount_code_id';
  public $fillable = ['shop','menu_id','owner_id','code','allot','used','per_use','min_price','max_price','discount_type','discount','discount_max','year','dayofweek','month','day','hour','minute','show_date','start_date','end_date','is_available','payment_method','normal_price'];
  protected $guarded = [];
  public $timestamps = true;
  protected $dates = ['deleted_at'];




  public function Owner($owner_id){

    $data_owner = DB::table('owner')
    ->select('company_name')
    ->where('owner_id',$owner_id)
    ->first();

    return $data_owner->company_name;
  }
  public function Shop($shop,$main_lang){
   $shop_name = '';
   $shop_id = explode(",",$shop);
   foreach ($shop_id as $value){
     $data_shop = DB::table('shop_tr')
     ->select('shop_name')
     ->where('shop_id',$value)
     ->where('lang',$main_lang)
     ->first();
     $name[] = $data_shop->shop_name;
   }
   return $name;
 }

public function Menu($menu_id,$main_lang){
   $menu_name = '';
   $menu_id = explode(",",$menu_id);
   foreach ($menu_id as $value){
     $data_menu = DB::table('menu_tr')
     ->select('menu_name')
     ->where('menu_id',$value)
     ->where('lang',$main_lang)
     ->first();
     $menu[] = $data_menu->menu_name;
   }
   return $menu;
 }
}

?>
