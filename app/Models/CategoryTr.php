<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryTr extends Model
{
    use SoftDeletes;
    public $table = 'category_tr';
    public $primaryKey = 'category_tr_id';
    public $fillable = ['category_id','category_name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
