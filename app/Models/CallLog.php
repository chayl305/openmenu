<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CallLog extends Model
{
    use SoftDeletes;
    public $table = 'call_log';
    public $primaryKey = 'call_log_id';
    public $fillable = ['waiter_id','tables_id','action_id','time_call','finish_time'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
