<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class BannerPromotion extends Model
{
    use SoftDeletes;
    public $table = 'banner_promotion';
    public $primaryKey = 'banner_promotion_id';
    public $fillable = ['shop','owner_id','img_name','is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

   public function Shop($shop,$main_lang){
       $shop_name = '';
       $shop_id = explode(",",$shop);
       foreach ($shop_id as $value){
           $data_shop = DB::table('shop_tr')
               ->select('shop_name')
               ->where('shop_id',$value)
               ->where('lang',$main_lang)
               ->first();
           $name[] = $data_shop->shop_name;
       }
        return $name;
    }
}
