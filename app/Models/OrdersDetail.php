<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class OrdersDetail extends Model
{
    use SoftDeletes;
    public $table = 'orders_detail';
    public $primaryKey = 'orders_detail_id';
    public $fillable = ['orders_id','menu_id','menu_option','qty','price','option_price'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Menu($menu_id,$main_lang){

        $data_menu = DB::table('menu_tr')
            ->select('menu_name')
            ->where('menu_id',$menu_id)
            ->where('lang',$main_lang)
            ->first();

        return $data_menu->menu_name;
    }

}
