<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Menu extends Model
{
    use SoftDeletes;
    public $table = 'menu';
    public $primaryKey = 'menu_id';
    public $fillable = ['shop_id','category_id','price','special_price','is_recommented','is_available','img_name','status'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Shop($shop_id,$main_lang){

        $data_shop = DB::table('shop_tr')
        ->select('shop_name')
        ->where('shop_id',$shop_id)
        ->where('lang',$main_lang)
        ->first();
        return $data_shop->shop_name;
    }

    public function Category($category_id,$main_lang){

        $data_category = DB::table('category')
        ->select('category_name')
        ->where('category_id',$category_id)
        ->where('lang',$main_lang)
        ->first();

        return $data_category->category_name;
    }
}
