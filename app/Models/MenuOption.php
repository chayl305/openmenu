<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class MenuOption extends Model
{
    use SoftDeletes;
    public $table = 'menu_option';
    public $primaryKey = 'menu_option_id';
    public $fillable = ['menu_id','option_group_id','option_value_id','price'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];


 public function Menu($menu_id,$main_lang){

        $data_menu = DB::table('menu_tr')
        ->select('menu_name')
        ->where('menu_id',$menu_id)
        ->where('lang',$main_lang)
        ->first();

        return $data_menu->menu_name;

    }
     public function OptionGroup($option_group_id,$main_lang){

        $data_option_group = DB::table('option_group_tr')
        ->select('name')
        ->where('option_group_id',$option_group_id)
        ->where('lang',$main_lang)
        ->first();

        return $data_option_group->name;
    }

    public function OptionValue($option_value_id,$main_lang){

        $data_option_value = DB::table('option_value_tr')
        ->select('name')
        ->where('option_value_id',$option_value_id)
        ->where('lang',$main_lang)
        ->first();

        return $data_option_value->name;
    }



}
