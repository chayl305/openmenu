<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetailMenu extends Model
{
    use SoftDeletes;
    public $table = 'order_detail_menu';
    public $primaryKey = 'orders_detail_menu_id';
    public $fillable = ['orders_detail_id','menu_name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
