<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Branch extends Model
{
    use SoftDeletes;
    public $table = 'branch';
    public $primaryKey = 'branch_id';
    public $fillable = ['shop_id','phone','address','contact_name','contact_phone','contact_email','kitchen_user','kitchen_password','is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Tables($branch_id){

        $count_tables = DB::table('tables')
        	->where('branch_id',$branch_id)
            ->select(DB::raw('COUNT(DISTINCT(tables_id)) as tables_id'))
            ->first();

        return $count_tables->tables_id;
    }
}
