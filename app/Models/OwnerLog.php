<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OwnerLog extends Model
{
    use SoftDeletes;
    public $table = 'owner_log';
    public $primaryKey = 'owner_log_id';
    public $fillable = ['owner_id','name','detail','date'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Owner($owner_id){

        $data_owner = DB::table('owner')
            ->select('company_name')
            ->where('owner_id',$owner_id)
            ->first();

        return $data_owner->owner_name;
    }
}
