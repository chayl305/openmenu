<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetailOption extends Model
{
    use SoftDeletes;
    public $table = 'orders_detail_option';
    public $primaryKey = 'orders_detail_option_id';
    public $fillable = ['orders_detail_id','option_name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
