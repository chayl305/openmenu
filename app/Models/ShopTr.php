<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopTr extends Model
{
    use SoftDeletes;
    public $table = 'shop_tr';
    public $primaryKey = 'shop_tr_id';
    public $fillable = ['shop_id','shop_name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
