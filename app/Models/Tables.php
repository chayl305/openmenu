<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Tables extends Model
{
    use SoftDeletes;
    public $table = 'tables';
    public $primaryKey = 'tables_id';
    public $fillable = ['branch_id','table_no','random_key'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Branch($branch_id,$main_lang){

        $data_branch = DB::table('branch_tr')
            ->select('branch_name')
            ->where('branch_id',$branch_id)
            ->where('lang',$main_lang)
            ->first();
        
        return $data_branch->branch_name;
    }

}
