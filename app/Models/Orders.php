<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Orders extends Model
{
    use SoftDeletes;
    public $table = 'orders';
    public $primaryKey = 'orders_id';
    public $fillable = ['shop_id','branch_id','tables_id','table_no','orders_no','total_price','discount_price','discount_id','code','payment_date'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Shop($shop_id,$main_lang){

        $data_shop = DB::table('shop_tr')
            ->select('shop_name')
            ->where('shop_id',$shop_id)
            ->where('lang',$main_lang)
            ->first();

        return $data_shop->shop_name;
    }

    public function Branch($branch_id,$main_lang){

        $data_branch = DB::table('branch_tr')
            ->select('branch_name')
            ->where('branch_id',$branch_id)
            ->where('lang',$main_lang)
            ->first();
        
        return $data_branch->branch_name;
    }

    public function Menu($menu_id,$main_lang){

        $data_menu = DB::table('menu_tr')
            ->select('menu_name')
            ->where('menu_id',$menu_id)
            ->where('lang',$main_lang)
            ->first();

        return $data_menu->menu_name;
    }

}
