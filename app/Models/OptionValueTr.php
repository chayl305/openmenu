<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionValueTr extends Model
{
    use SoftDeletes;
    public $table = 'option_value_tr';
    public $primaryKey = 'option_value_tr_id';
    public $fillable = ['option_value_id','name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
