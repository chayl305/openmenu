<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class MenuTr extends Model
{
    use SoftDeletes;
    public $table = 'menu_tr';
    public $primaryKey = 'menu_tr_id';
    public $fillable = ['menu_id','menu_name','menu_description','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
