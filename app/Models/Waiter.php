<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Waiter extends Model
{
    use SoftDeletes;
    public $table = 'waiter';
    public $primaryKey = 'waiter_id';
    public $fillable = ['shop_id','branch','waiter_name','phone','username','password'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Branch($branch,$main_lang){
       $branch_name = '';
       $branch_id = explode(",",$branch);
       foreach ($branch_id as $value){
           $data_branch = DB::table('branch_tr')
               ->select('branch_name')
               ->where('branch_id',$value)
               ->where('lang',$main_lang)
               ->first();
           $name[] = $data_branch->branch_name;
       	}
        return $name;
    }
}
