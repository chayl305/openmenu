<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    use SoftDeletes;
    public $table = 'language';
    public $primaryKey = 'language_id';
    public $fillable = ['country','lang','is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
