<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessTypeTr extends Model
{
    use SoftDeletes;
    public $table = 'business_type_tr';
    public $primaryKey = 'business_type_tr_id';
    public $fillable = ['business_type_id','name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
