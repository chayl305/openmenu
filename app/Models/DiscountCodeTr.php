<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCodeTr extends Model
{
    use SoftDeletes;
    public $table = 'discount_code_tr';
    public $primaryKey = 'discount_code_tr_id';
    public $fillable = ['discount_code_id','code_name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
