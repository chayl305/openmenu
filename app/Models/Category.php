<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Category extends Model
{
    use SoftDeletes;
    public $table = 'category';
    public $primaryKey = 'category_id';
    public $fillable = ['shop_id','img_name','is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Shop($shop_id,$main_lang){

        $data_shop = DB::table('shop_tr')
            ->select('shop_name')
            ->where('shop_id',$shop_id)
            ->where('lang',$main_lang)
            ->first();

        return $data_shop->shop_name;
    }
}
