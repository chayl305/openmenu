<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Shop extends Model
{
    use SoftDeletes;
    public $table = 'shop';
    public $primaryKey = 'shop_id';
    public $fillable = ['owner_id','business_type_id','address','phone','website','contact_name','contact_phone','contact_email','img_logo','is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Owner($owner_id){

        $data_owner = DB::table('owner')
            ->select('company_name')
            ->where('owner_id',$owner_id)
            ->first();

        return $data_owner->company_name;
    }
}
