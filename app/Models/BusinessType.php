<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessType extends Model
{
    use SoftDeletes;
    public $table = 'business_type';
    public $primaryKey = 'business_type_id';
    public $fillable = ['','',''];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
