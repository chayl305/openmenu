<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchTr extends Model
{
    use SoftDeletes;
    public $table = 'branch_tr';
    public $primaryKey = 'branch_tr_id';
    public $fillable = ['branch_id','branch_name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
