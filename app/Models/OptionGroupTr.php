<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionGroupTr extends Model
{
    use SoftDeletes;
    public $table = 'option_group_tr';
    public $primaryKey = 'option_group_tr_id';
    public $fillable = ['option_group_id','name','lang'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
