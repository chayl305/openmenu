<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Language;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $bo_name = config()->get('constants.BO_NAME');
            $lang = session()->get('locale');

            $available_lang = Language::select('lang','main_lang')->where('is_available','1')->get();

            $main_lang = Language::select('lang')->where('main_lang','1')->where('is_available','1')->first();
            $main_lang = $main_lang->lang;

            $view->with(['bo_name' => $bo_name, 'lang' => $lang, 'available_lang' => $available_lang, 'main_lang'=>$main_lang]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
