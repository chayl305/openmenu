<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

if(!empty(input::get('branch_id'))){
    $branch_id = "?branch_id=".input::get('branch_id');
}else{
    $branch_id = "";
}
?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
                <div class="text-right">
                    <div>
                        <a href="{{URL::to($path.'?mode=print'.$str_param)}}" target="_blank"><button class="btn btn-circle blue btn-sm" type="submit"><i class="fa fa-print"></i> Print</button></a>
                        @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                            <th class="text-center col-sm-2 col-md-2">
                                @if($permission['c'] == '1')
                                    <a href="{{ url()->to($path.'/create'.$branch_id) }}" class="btn btn-circle blue btn-sm"><i class="fa fa-plus"></i> Add</a>
                                @endif
                            </th>
                        @endif
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                {!! Form::open(array('url' => '_admin/del_tables' , 'method' => 'post' ,'id'=>'form-tag-product', 'class' => 'form-horizontal')) !!}
                <div>
                    @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                        @if($permission['d'] == '1')
                            <button class="btn btn-sm btn-circle red" name="delete" type="submit" value="delete"><i class="fa fa-trash-o"> Delete </i></button>
                        @endif
                    @endif
                </div><br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1"><input type="checkbox" name="selectedAll" id="selectedAll"></th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-4">{!! $obj_fn->sorting('Table No.','table_no',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-4">{!! $obj_fn->sorting('Zone','branch_id',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="span1">
                                            <div class="text-center">
                                                <input type="checkbox" name="table_chk[]" id="table_chk[]" class="checkbox1" value="{{ $field->$primaryKey }}">
                                            </div>
                                        </td>
                                        {!! Form::close() !!}
                                        <td class="text-center">{{  $field->$primaryKey }}</td>
                                        <td class="text-center">
                                            {!! Form::open(array('url'=>'_admin/sequence/table' , 'method' => 'post' , 'id' => 'form' , 'class' => 'form-horizontal'  )) !!}
                                            <input type="hidden" name="branch_id" value="{{$field->branch_id}}">
                                            <div class="input-group">
                                                <input type="text" class="form-control text-center" name="table_no" value="{{$field->table_no}}">
                                                <div class="input-group-btn">
                                                    <input type="hidden" name="{{$primaryKey}}" value="{{$field->$primaryKey}}">
                                                    <button class="btn" type="submit"><i class="fa fa-pencil-square-o "></i></button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </td>
                                        <td class="text-center">{{ $field->Branch($field->branch_id,$main_lang) }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="5">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('js/change-status.js') }}
@endsection
@section('more-script')
    <script>
        $(document).ready(function() {
            $('#selectedAll').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox1').attr('checked', 'checked');
                    $('.checkbox1').parent().addClass('checked');
                }else{
                    $('.checkbox1').removeAttr('checked');
                    $('.checkbox1').parent().removeClass('checked');
                }
            });
        });
    </script>
@endsection