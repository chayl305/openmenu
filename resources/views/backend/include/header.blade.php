<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{ url()->to($bo_name) }}"><img src="{{ url()->asset('assets/backend/layout3/img/logo-default.png') }}" alt="logo" class="logo-default"></a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu hidden-xs">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-extended">
                        <a href="#">{{ session()->get('s_admin_name') }}</a>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <li class="droddown dropdown-separator"><span class="separator"></span></li>
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-extended">
                        <a href="{{ url()->to($bo_name.'/logout') }}"><i class="icon-logout"></i> Logout</a>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->

    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ url()->to($bo_name) }}">Home</a>
                    </li>
                    @if(empty(session()->get('s_owner_id')) && empty(session()->get('s_shop_id')))
                        <li>
                            <a href="{{ url()->to($bo_name.'/owner') }}">Owner</a>
                        </li>
                    @endif
                    @if(!empty(session()->get('s_owner_id')))
                        <li>
                            <a href="{{ url()->to($bo_name.'/owner') }}">Profile</a>
                        </li>
                        <li>
                            <a href="{{ url()->to('_admin/banner_promotion?owner_id='.session()->get('s_owner_id')) }}">Banner Promotion</a>
                        </li>
                        <li>
                            <a href="{{ url()->to('_admin/discount_code?owner_id='.session()->get('s_owner_id')) }}">Discount Code</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ url()->to($bo_name.'/shop') }}">Site</a>
                    </li>
                    @if(empty(session()->get('s_owner_id')))
                        <li>
                            <a href="{{ url()->to($bo_name.'/branch') }}">Zone</a>
                        </li>
                    @endif
                    <!-- <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            Area <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">

                            <li>
                                <a href="{{ url()->to($bo_name.'/branch') }}">Branch</a>
                            </li>
                            <li>
                                <a href="{{ url()->to($bo_name.'/tables') }}">Table</a>
                            </li>
                            <li>
                                <a href="{{ url()->to($bo_name.'/category') }}">Category</a>
                            </li>
                            <li>
                                <a href="{{ url()->to($bo_name.'/menu') }}">Menu</a>
                            </li>
                            <li>
                                <a href="{{ url()->to($bo_name.'/option_group') }}">Option Group</a>
                            </li>
                            <li>
                                <a href="{{ url()->to($bo_name.'/option_value') }}">Option Value</a>
                            </li>
                        </ul>
                    </li> -->
                    <li>
                        <a href="{{ url()->to($bo_name.'/orders') }}">Order</a>
                    </li>

                    @if(empty(session()->get('s_owner_id')))
                        @if(empty(session()->get('s_shop_id')))
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                    User Management <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-left">

                                    <li>
                                        <a href="{{ url()->to($bo_name.'/user-management') }}">User</a>
                                    </li>
                                    <li>
                                        <a href="{{ url()->to($bo_name.'/role') }}">Role</a>
                                    </li>
                                    <li>
                                        <a href="{{ url()->to($bo_name.'/page') }}">Page</a>
                                    </li>
                                    <!-- <li>
                                        <a href="{{ url()->to($bo_name.'/permission') }}">Permission</a>
                                    </li> -->
                                </ul>
                            </li>
                        @endif
                    @endif
                    @if(!empty(session()->get('s_owner_id')))
                        <li>
                            <a href="{{ url()->to('_admin/change_password?owner_id='.session()->get('s_shop_id')) }}">Change Possword</a>
                        </li>
                    @endif
                    @if(!empty(session()->get('s_shop_id')))
                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                Other <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-left">

                                <li>
                                    <a href="{{ url()->to('_admin/category?shop_id='.session()->get('s_shop_id')) }}">Category</a>
                                </li>
                                <li>
                                    <a href="{{ url()->to('_admin/menu?shop_id='.session()->get('s_shop_id')) }}">Menu</a>
                                </li>
                                <li>
                                    <a href="{{ url()->to('_admin/option_group?shop_id='.session()->get('s_shop_id')) }}">Option Group</a>
                                </li>
                                <li>
                                    <a href="{{ url()->to('_admin/option_value?shop_id='.session()->get('s_shop_id')) }}">Option Value</a>
                                </li>
                                <li>
                                    <a href="{{ url()->to('_admin/kitchen_user?shop_id='.session()->get('s_shopr_id')) }}">Kitchen User</a>
                                </li>
                                <li>
                                    <a href="{{ url()->to('_admin/waiter_user?shop_id='.session()->get('s_shop_id')) }}">Waiter User</a>
                                </li>
                                <!-- <li>
                                    <a href="{{ url()->to('_admin/change_password?shop_id='.session()->get('s_shop_id')) }}">Change Password</a>
                                </li> -->
                            </ul>
                        </li>
                    @endif

                    <li class="visible-xs">
                        <a href="{{ url()->to($bo_name.'/logout') }}">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
