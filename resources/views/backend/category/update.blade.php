<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$shop_id = Input::get('shop_id');

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    @if(!empty(Input::get('shop_id')))
                        <input type="hidden" name="shop_id" value="{{ Input::get('shop_id') }}">
                    @endif

                    <div class="form-body">
                        @foreach($available_lang as $value)
                            <div class="form-group">
                                <label class="control-label col-md-3">Category Name [ {{ strtoupper($value['lang']) }} ]</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="category_name[{{ $value['lang'] }}]" @if($method=='PUT' and !empty($category_name[$value['lang']])) value="{{ $category_name[$value['lang']] }}" @endif required>
                                </div>
                            </div>
                        @endforeach
                        <?php
                            $field = 'img_name';
                            $label = 'Image';
                            $upload_path = 'category';
                        ?>
                        <div id="bg-img">
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="img_thumbnail">
                                        @if($$field != '')
                                            <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                        @else
                                            <img src="" class="img_path" style="max-width:400px;">
                                        @endif

                                        <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">{{ $label }}</label>
                                <div class="col-md-4">
                                    @if($$field != '')
                                        <div class="btn btn-danger remove_img" >Remove</div>
                                        <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                    @elseif ($$field == '')
                                        <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                        <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                    @endif

                                    <input type="hidden" name="img_del" class="img_del" value="n">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-4">
                                    <span style="color:#F00">* หมายเหตุ</span><br>
                                        <span style="">
                                            - รูปควรมีขนาด 375 x 249 pixel. <br>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/validation.js') }}
@endsection