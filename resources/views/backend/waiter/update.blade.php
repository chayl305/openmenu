<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$shop_id= Input::get('shop_id');

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">

                    @if(!empty(Input::get('shop_id')))
                        <input type="hidden" name="shop_id" value="{{ Input::get('shop_id') }}">
                    @endif

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Zone</label>
                            <div class="col-md-4">
                                <select  name="branch[]" class="form-control select2-container form-control select2me " multiple="multiple" id="branch" required>
                                    @foreach($data_branch as $key => $value)
                                        <?php $selected = "";?>
                                        @if(!empty($branch_name[$value->branch_id]->branch_name))
                                            @if($branch_name[$value->branch_id]->branch_name == $value->branch_name)
                                                <?php $selected = "selected"; ?>
                                            @endif
                                        @endif
                                            <option value="{{ $value->branch_id }}" {{ $selected }}>{{ $value->branch_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Waiter Name</label>
                            <div class="col-md-4">
                            <input type="text" class="form-control" name="waiter_name" value="{{ $waiter_name }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-4">
                            <input type="text" class="form-control" name="phone" value="{{ $phone }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-4">
                                <div class="input-group" style="text-align:left">
                                    <input type="text" class="form-control" name="username" id="username_input" value="{{ $username }}">
                                    <span class="input-group-btn">
                                    <a href="javascript:;" class="btn green" id="username_checker">
                                        <i class="fa fa-check"></i> Check </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @if($method != 'PUT')
                        <div class="form-group last password-strength">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="password" id="password_strength">
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green" @if($txt_manage == 'Add') disabled="disabled" @endif>{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/waiter/validation.js') }}
    {{ Html::script('js/backend/waiter/more.js') }}
@endsection
