<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;


$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$shop_id = Input::get('shop_id');
if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                      @if(!empty(Input::get('shop_id')))
                        <input type="hidden" name="shop_id" value="{{ Input::get('shop_id') }}">
                    @endif

                    

                  <!--   <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Site</label>
                            <div class="col-md-4">
                                <select name="shop_id" class="form-control">
                                    @foreach($data_shop as $value)
                                        <option value="{{ $value->shop_id }}" @if($value->shop_id == $shop_id) selected @endif >{{ $value->shop_name }}</option>{{$value->shop_name}}
                                    @endforeach
                                </select>
                            </div>
                        </div> -->
                    
                        @foreach($available_lang as $value)
                            <div class="form-group">
                                <label class="control-label col-md-3">Option Value Name [ {{ strtoupper($value['lang']) }} ]</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="name[{{ $value['lang'] }}]" @if($method=='PUT' and !empty($name[$value['lang']])) value="{{ $name[$value['lang']] }}" @endif required="">
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/role/validation.js') }}
@endsection