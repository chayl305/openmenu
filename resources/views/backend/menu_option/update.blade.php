<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;
$menu_id = Input::get('menu_id');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}

?>
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    @if(!empty(Input::get('menu_id')))
                        <input type="hidden" name="menu_id" value="{{ Input::get('menu_id') }}">
                    @endif
                    <div class="form-body">

                        @if(empty(Input::get('option_group_id')))
                            <div class="form-group">
                                <label class="control-label col-md-3">Option Group</label>
                                <div class="col-md-4">
                                    <select name="option_group_id" class="form-control select2 select2me" required>
                                        <option value="">Select......</option>
                                        @foreach($data_option_group as $value)
                                            <option value="{{ $value->option_group_id }}" @if($value->option_group_id == $option_group_id) selected @endif >{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif

                            @if(empty(Input::get('option_group_id')))
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option Value</label>
                                    <div class="col-md-4">
                                        <select name="option_value_id" class="form-control select2 select2me" required>
                                            <option value="">Select......</option>
                                            @foreach($data_option_value as $value)
                                                <option value="{{ $value->option_value_id }}" @if($value->option_value_id == $option_value_id) selected @endif >{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif

                        <div class="form-group">
                            <label class="control-label col-md-3">Price</label>
                            <div class="col-md-2">
                                <input type="number" class="form-control" name="price" min="0.00" step="0.00" value="{{ $price }}" required="">
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/validation.js') }}
    {{ Html::script('js/backend/theme_component/validation.js') }}
@endsection