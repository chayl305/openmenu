<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

// if(isset($data)) {
//     foreach($fillable as $value){
//         $$value = $data->$value;
//     }
// } else {
//     foreach($fillable as $value){
//         $$value = "";
//     }
// }
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">ID</th>
                            <th class="text-center col-sm-2" >Orders Detail ID</th>
                            <th class="text-center" >Option Name</th>
                            <th class="text-center col-sm-1" >Language</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $field)
                                <tr>
                                    <td class="text-center">{{ $field->$primaryKey }}</td>
                                    <td class="text-center">{{ $field->orders_detail_id }}</td>
                                    <td>{{ $field->option_name }}</td>
                                    <td class="text-center">{{ $field->lang }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/admin/validation.js') }}
    {{ Html::script('js/backend/admin/more.js') }}
@endsection