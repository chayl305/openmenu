<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');
$shop =  '';
$dayofweek = array("อาทิตย์","จันทร์","อังคาร์","พุธ","พฤหัสบดี","ศุกร์","เสาร์"); 
$month=array(    
    "1"=>"มกราคม",  
    "2"=>"กุมภาพันธ์",  
    "3"=>"มีนาคม",  
    "4"=>"เมษายน",  
    "5"=>"พฤษภาคม",  
    "6"=>"มิถุนายน",   
    "7"=>"กรกฎาคม",  
    "8"=>"สิงหาคม",  
    "9"=>"กันยายน",  
    "10"=>"ตุลาคม",  
    "11"=>"พฤศจิกายน",  
    "12"=>"ธันวาคม"                    
    );  



$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);
if(!empty(input::get('owner_id'))){
    $owner_id = "?owner_id=".input::get('owner_id');
}else{
    $owner_id = "";
}

?>
@extends('backend.layout.main-layout')

@section('page-style')
{{ Html::style('css/change-status.css') }}
@endsection

@section('more-style')
@endsection

@section('page-title')
{{ $page_title }}
@endsection

@section('page-content')
<div class="col-md-12">
    <div class="portlet light">
        <div class="form-search">
            <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                @if(!empty(Input::get('owner_id')))
                <input type="hidden" name="owner_id" value="{{ Input::get('owner_id') }}">
                @endif
                <div class="form-group">
                    <label class="control-label col-md-1">Search</label>
                    <div class="col-md-3">
                        <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                        <span class="help-block">Search by Code Name</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-3 ">
                        <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
            </form>
            <hr>
        </div>

        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-database font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
            </div>
        </div>
        <!-- <div class="col-md-offset-10"><b>|*|</b>หมายถึงสามารถใช้ได้ทุกเวลา</div><br> -->
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-1 text-center">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-md-1">{!! $obj_fn->sorting('Code','code',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-md-2">{!! $obj_fn->sorting('Code Name','code_name',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-2">Owner{{-- {!! $obj_fn->sorting('Owner','company_name',$order_by,$sort_by,$str_param_sort,'') !!} --}}</th>
                            <th class="col-md-2 text-center">
                                <span>Menu</span>
                            </th>
                            <th class="col-md-2 text-center">
                                <span class="font-green">Allot</span> / <span class="font-yellow">Used</span>
                            </th>
                            <th class="col-md-4 text-center">
                                <span class="font-blue">Duration</span>
                            </th>
                            <th class="col-md-1">{!!('Status') !!}</th>
                            @if ($permission['u'] == '1')
                            <th class="col-md-1">{!! $obj_fn->sorting('Available','is_available',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            @endif
                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                            <th class="text-center col-sm-2 col-md-2">
                                @if($permission['c'] == '1')
                                <a href="{{ url()->to($path.'/create'.$owner_id) }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                @endif
                            </th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if($count_data > 0)
                        @foreach($data as $key => $field)
                        <?php
                        $txt = $field->minute.' '.$field->hour.' '.$field->day.' '.$field->month.' '.$field->dayofweek.' '.$field->year;
                        ?>
                        <tr>
                            <td class="text-center">{{ $field->$primaryKey }}</td>
                            <td>{{ $field->code }}</td>
                            <td>{{ $field->code_name }}</td>
                            <td>{{ $field->company_name}}<br>
                                @foreach($field->Shop($field->shop,$main_lang) as $key => $value)
                                &nbsp;&nbsp;-&nbsp;{{$value}}<br>                                             
                                @endforeach

                            </td>   
                            <td> @foreach($field->Menu($field->menu_id,$main_lang) as $key => $value)
                                &nbsp;&nbsp;-&nbsp;{{$value}}<br>                                             
                                @endforeach</td>      
                            <td class="text-center">
                                <span class="font-green">{{ $field->allot}}</span> / <span class="font-yellow">{{ $field->used }}</span> 
                            </td>
                            <td class="text-left">
                                <span class="font-black"><b>ปี :: 
                                    @if($field->year == "*")
                                    <span class="font-black"><b>ทุกปี</b></span>
                                    @else
                                    <span class="font-black"><b>{{$field->year}}</b></span>
                                    @endif
                                </b></span><br>

                                <span class="font-black"><b>วันในสัปดาห์ :: 
                                    @if($field->dayofweek == "*")
                                    <span class="font-black"><b>ทุกสัปดาห์</b></span>
                                    @else
                                    <?php
                                    $week = explode("-",$field->dayofweek);
                                    $day = '';
                                    ?>
                                    @foreach($week as $key => $value)
                                    <?php $day .= $dayofweek[$value].' - '; ?>
                                    @endforeach
                                    {{substr($day,0,-3)}}
                                    @endif

                                    <b></span><br>

                                        <span class="font-black"><b>เดือน :: 
                                            @if($field->month == "*")

                                            <span class="font-black"><b>ทุกเดือน</b></span>
                                            @else
                                            <?php
                                            $months = explode("-",$field->month);
                                            $monthm = '';


                                            ?>
                                            @foreach($months as $key => $value)
                                            <?php $monthm .= $month[$value].' - '; ?>
                                            @endforeach
                                            {{substr($monthm,0,-3)}}
                                            @endif
                                        </b></span><br>

                                        <span class="font-black"><b>วันที :: 
                                            @if($field->day == "*")
                                            <span class="font-black"><b>ทุกวัน</b></span>
                                            @else
                                            <span class="font-black"><b>{{$field->day}}&nbsp;วัน</b></span>
                                            @endif
                                        </b></span><br>
                                        <span class="font-black"><b>เวลา :: 
                                            @if($field->hour == "*")
                                            <span class="font-black"><b>ทุกเวลา</b></span>
                                            @else
                                            <span class="font-black"><b>{{$field->hour}}&nbsp;นาฬิกา</b></span>
                                            @endif
                                        </b></span><br>
                                        <span class="font-black"><b>นาที :: 
                                            @if($field->minute == "*")
                                            <span class="font-black"><b>ทุกนาที</b></span>
                                            @else
                                            <span class="font-black"><b>{{$field->minute}}&nbsp;นาที</b></span>
                                            @endif
                                        </b></span><br>
                                    </td>




                                    <td class="text-center"> 
                                        @if($obj_fn->parse_crontab($txt))
                                        <span class="font-green"><b>ส่วนลดสามารถใช่ได้</b></span>
                                        @else
                                        <span class="font-red"><b>ส่วนลดไม่สามารถใช่ได้</b></span>  
                                        @endif
                                    </td>
                                    @if ($permission['u'] == '1')
                                    @if($field->is_available == 0 )
                                    <td><div class="text-center"><a id="is_available-{{$field->$primaryKey}}" class=" change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="is_available"  data-value="{{$field->is_available}}"></a></div></td>
                                    @else
                                    <td><div class="text-center"><a id="is_available-{{$field->$primaryKey}}" class=" change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="is_available"  data-value="{{$field->is_available}}"></a></div></td>
                                    @endif
                                    @endif
                                    @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                    <td class="text-center">
                                        @if($permission['u'] == '1')
                                        <a href="{{ url()->to($path.'/'.$field->$primaryKey.'/edit?1'.$str_param) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                        @endif
                                        @if($permission['d'] == '1')
                                        <form action="{{ url()->to($path.'/'.$field->$primaryKey) }}" class="form-delete" parent-data-id="{{ $field->$primaryKey }}" method="POST" >
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="button" class="btn btn-xs btn-circle red btn-delete" data-id="{{ $field->$primaryKey }}"><i class="fa fa-trash-o"></i></button>
                                        </form>
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-center" colspan="12">No Result.</td>
                                </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                    {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
        @endsection

        @section('page-plugin')
        {{ Html::script('js/change-status.js') }}
        @endsection
        @section('more-script')
        @endsection

