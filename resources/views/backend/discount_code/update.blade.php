<?php

$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$owner_id = Input::get('owner_id');
if(!empty(input::get('shop_id'))){
    $shop_id = "?shop_id=".input::get('shop_id');
}else{
    $shop_id = "";
}

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}

?>

@extends('backend.layout.main-layout')
@section('page-style')
{{ Html::style('assets/global/plugins/select2/select2.css') }}
{{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
{{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
{{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
{{ Html::style('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}
{{ Html::style('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
{{ Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}

@endsection
@section('more-style')
@endsection

@section('page-title')
{{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')

<div class="col-md-12">
    <div class="portlet light">
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                <input type="hidden" name="_method" value="{{ $method }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="str_param" value="{{ $str_param }}">
                @if(!empty(Input::get('owner_id')))
                <input type="hidden" name="owner_id" value="{{ Input::get('owner_id') }}">
                @endif
                <div class="form-body">
                    <!-- @if(empty(Input::get('owner_id')))
                        <div class="form-group">
                            <label class="control-label col-md-3">Owner</label>
                            <div class="col-md-4">
                                <select name="owner_id" class="form-control" required>
                                    @foreach($data_owner as $value)
                                        <option value="{{ $value->owner_id }}" @if($value->owner_id == $owner_id) selected @endif >{{ $value->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif -->

                        <div class="form-group">
                            <label class="control-label col-md-3">Site</label>
                            <div class="col-md-4">
                                <select  name="shop[]" class="form-control select2-container form-control select2me " multiple="multiple" id="shop" required>
                                    @foreach($data_shop as $key => $value)
                                    <?php $selected = "";?>
                                    @if(!empty($shop_name[$value->shop_id]->shop_name))
                                    @if($shop_name[$value->shop_id]->shop_name == $value->shop_name)
                                    <?php $selected = "selected"; ?>
                                    @endif
                                    @endif
                                    <option value="{{ $value->shop_id }}" {{ $selected }}>{{ $value->shop_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Menu</label>
                            <div class="col-md-4">
                                <select  name="menu_id[]" class="form-control select2-container form-control select2me " multiple="multiple" id="menu_id" required>
                                    @foreach($data_menu as $key => $value)
                                    <?php $selected = "";?>
                                    @if(!empty($menu_name[$value->menu_id]->menu_name))
                                    @if($menu_name[$value->menu_id]->menu_name == $value->menu_name)
                                    <?php $selected = "selected"; ?>
                                    @endif
                                    @endif
                                    <option value="{{ $value->menu_id }}" {{ $selected }}>{{ $value->menu_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Code </label>
                            <div class="col-md-4">
                                <input type='text'  name="code" id="code" class="form-control" value="{{ $code }}" required>
                            </div>
                        </div>

                        @foreach($available_lang as $value)

                        <div class="form-group">
                            <label class="control-label col-md-3">Code Name [ {{ strtoupper($value['lang']) }} ]</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="code_name[{{ $value['lang'] }}]" @if($method=='PUT' and !empty($code_name[$value['lang']])) value="{{ $code_name[$value['lang']] }}" @endif required>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>

                        @endforeach
                        <div class="form-group">
                            <label class="control-label col-md-3">Allot</label>
                            <div class="col-md-2">
                                <input type='number' min="0" name="allot" id="allot" class="form-control" value="{{ $allot }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Discount Type</label>
                            <div class="col-md-4">
                                <select class="form-control" name="discount_type" required>
                                    <option value="" selected>Select.....</option>
                                    <option value="fix" @if($discount_type == "fix") selected @endif >Fix</option>
                                    <option value="%"@if($discount_type == "%") selected @endif >%</option>
                                </select>
                            </div>
                        </div>

                        
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3">Used</label>
                        <div class="col-md-2">
                            <input type='number' min="0" name="used" id="used" class="form-control" value="{{ $used }}">
                        </div>
                    </div> -->

                   <!--  <div class="form-group">
                        <label class="control-label col-md-3">Per Use</label>
                        <div class="col-md-2">
                            @if($per_use != '')
                            <input type='number' min="0" name="per_use" id="per_use" class="form-control" value="{{$per_use}}">
                            @else
                            <input type='number' min="0" name="per_use" id="per_use" class="form-control" value="<?php echo 1?>">
                            @endif
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Discount</label>
                        <div class="col-md-2">
                            <input type='number' min="0" name="discount" id="discount" class="form-control" value="{{ $discount }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Discount Max</label>
                        <div class="col-md-2">
                            <input type='number' min="0" name="discount_max" id="discount_max" class="form-control" value="{{ $discount_max }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Minimum Price</label>
                        <div class="col-md-2">
                            <input type='number' min="0" name="min_price" id="min_price" class="form-control" value="{{ $min_price }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Maximum Price</label>
                        <div class="col-md-2">
                            <input type='number' min="0" name="max_price" id="max_price" class="form-control" value="{{ $max_price }}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-1">Year</label>
                        <div class="col-md-1">
                            @if($year != '')
                            <input type='text'  name="year" id="year" class="form-control" value="{{$year}}">
                            @else
                            <input type='text'  name="year" id="year" class="form-control" value="<?php echo "*"?>">
                            @endif

                        </div>
                        <label class="control-label col-md-1">Day of Week</label>
                        <div class="col-md-1">
                            @if($dayofweek != '')
                            <input type='text'  name="dayofweek" id="dayofweek" class="form-control" value="{{$dayofweek}}">
                            @else
                            <input type='text'  name="dayofweek" id="dayofweek" class="form-control" value="<?php echo "*"?>">
                            @endif
                        </div>

                        <label class="control-label col-md-1">Month</label>
                        <div class="col-md-1">
                            @if( $month != '')
                            <input type='text'  name="month" id="month" class="form-control" value="{{$month}}">
                            @else
                            <input type='text'  name="month" id="month" class="form-control" value="<?php echo "*"?>">
                            @endif
                        </div>

                        <label class="control-label col-md-1">Day</label>
                        <div class="col-md-1">
                            @if( $day!= '')
                            <input type='text'  name="day" id="day" class="form-control" value="{{$day}}">
                            @else
                            <input type='text'  name="day" id="day" class="form-control" value="<?php echo "*"?>">
                            @endif

                        </div>

                        <label class="control-label col-md-1">Hour</label>
                        <div class="col-md-1">
                            @if($hour != '')
                            <input type='text'  name="hour" id="hour" class="form-control" value="{{$hour}}">
                            @else
                            <input type='text'  name="hour" id="hour" class="form-control" value="<?php echo "*"?>">
                            @endif
                        </div>

                        <label class="control-label col-md-1">Minute</label>
                        <div class="col-md-1">
                            @if($minute != '')
                            <input type='text'  name="minute" id="minute" class="form-control" value="{{$minute}}">
                            @else
                            <input type='text'  name="minute" id="minute" class="form-control" value="<?php echo "*"?>">
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 text-left" > 
                           <b>รูปแบบของคำสั่งมีดังนี้</b><br>
                           Year &nbsp;&nbsp;&nbsp; คือ ปี หรือ ช่วงปี ระบุเป็น (คศ.) เช่น 2016 หรือ 2016-2017<br>
                           Day of Week &nbsp;&nbsp;&nbsp; คือ วันของแต่ละสัปดาห์ มีค่า 0 - 6 ดังนี้ (0 = อาทิตย์, 1 = จันทร์, 2 = อังคาร, 3 = พุธ,4 = พฤหัส,5 = ศุกร์,6 = เสาร์ ) เช่น 1-2 หมายถึง วันจันทร์และอังคารเท่านั้น, 6 หมายถึงวันเสาร์เท่านั้น<br>
                           Month &nbsp;&nbsp;&nbsp;คือ เดือน หรือ ช่วงเดือน มีค่า 1 – 12
                           เช่น 12 หมายถึง เดือนธันวาคม, 1-2 หมายถึง ตั้งแต่เดือน มกราคม ถึง กุมภาพันธ์
                           <br>
                           Day &nbsp;&nbsp;&nbsp;  คือ วันที่ มีค่า 1 - 31 เช่น 5 หมายถึง วันที่ 5, 10-15 หมายถึง วันที่ 10-15<br>
                           Hour &nbsp;&nbsp;&nbsp; คือ ชั่วโมง มีค่า 0 - 23 เช่น 17 หมายถึง ห้าโมงเย็น, 9-11 หมายถึง เก้าโมง ถึง สิบเอ็ดโมง<br>
                           Minute &nbsp;&nbsp;&nbsp;คือ นาที มีค่า 0 - 59 เช่น 30 หมายถึง นาทีที่ 30, 15-30 หมายถึง ช่วงนาทีที่ 15-30 นาที<br><br>
                           <span class="font-red"><b>หมายเหตุ</b><br>  
                               - หากต้องการให้ใช้ได้ทุกช่วงเวลา ทุกเดือน ทุกวัน หรือ ทุกปี ให้กำหนดด้วย *<br>
                               - สามารถใช้ตัวเลข เครื่องหมาย - และ * ในการกำหนดค่าเท่านั้นค่ะ
                           </span><br>
                       </div>
                   </div>
                   <div class="form-group">
                       <div class="col-md-12 text-left">
                           <h4 class="font-red"><b>Example</b></h4>
                           <span><b>1.ให้โค้ดส่วนลดสามารถใช้งานได้ในทุกๆปี เฉพาะวันจันทร์ - วันศุกร์ ในเดือนธันวาคม ช่วงเวลา 16.00 - 17.00 น. กำหนดดังนี้</b></span><br><br>
                           <table class="table table-striped table-bordered table-hover">
                             <thead>
                               <tr>
                                   <th class="text-center"><b>Year</b></th>
                                   <th class="text-center"><b>Day of Week</b></th>
                                   <th class="text-center"><b>Month</b></th>
                                   <th class="text-center"><b>Day</b></th>
                                   <th class="text-center"><b>Hour</b></th>
                                   <th class="text-center"><b>Minute</b></th>
                               </tr>
                               <tr>
                                   <th class="text-center"><b>*</b></th>
                                   <th class="text-center"><b>1-5</b></th>
                                   <th class="text-center"><b>12</b></th>
                                   <th class="text-center"><b>*</b></th>
                                   <th class="text-center"><b>16-17</b></th>
                                   <th class="text-center"><b>*</b></th>
                               </tr>
                           </thead>
                       </table>
                   </div>
               </div>
               <div class="form-group">
                   <div class="col-md-12 text-left">
                       <h4 class="font-red"><b>Example</b></h4>
                       <span><b>2.จะให้โค้ดส่วนลดสามารถใช้งานได้เฉพาะปี 2016 - 2017 เเละเฉพาะวันที่ 1 - 5 ของทุกเดือน ช่วงเวลา 09.00 - 09.45 น. กำหนดดังนี้</b></span><br>
                       <table class="table table-striped table-bordered table-hover">
                         <thead>
                           <tr>
                               <th class="text-center"><b>Year</b></th>
                               <th class="text-center"><b>Day of Week</b></th>
                               <th class="text-center"><b>Month</b></th>
                               <th class="text-center"><b>Day</b></th>
                               <th class="text-center"><b>Hour</b></th>
                               <th class="text-center"><b>Minute</b></th>
                           </tr>
                           <tr>
                               <th class="text-center"><b>2016-2017</b></th>
                               <th class="text-center"><b>*</b></th>
                               <th class="text-center"><b>*</b></th>
                               <th class="text-center"><b>1-5</b></th>
                               <th class="text-center"><b>9</b></th>
                               <th class="text-center"><b>0-45</b></th>
                           </tr>
                       </thead>
                   </table>
               </div>
           </div>
           <br><br>
           <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                   <input type='hidden' min="0" name="per_use" id="per_use" class="form-control" value="1">
                   <button type="submit" class="btn green">{{ $txt_manage }}</button>
                   <button type="reset" class="btn default">Reset</button>
               </div>
           </div>
       </div>
   </div>
</form>
</div>
</div>

</div>

@endsection
@section('page-plugin')

{{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
{{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
{{ Html::script('assets/global/plugins/select2/select2.min.js') }}
{{ Html::script('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
{{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
{{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
{{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
{{ Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
{{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}

@endsection
@section('more-script')
{{ Html::script('js/backend/role/validation.js') }}
{{ Html::script('js/backend/theme_component/validation.js') }}
{{ Html::script('js/backend/discount_code/validation.js') }}
{{ Html::script('js/backend/discountcode/validation.js') }}
<script>
    FormValidation.init();
    ComponentsPickers.init();
     // function del_site(id) {
     //        $('#shop_text_'+id).hide();
     //        $('#shop_'+id).val('');
     //    }
 </script>
 @endsection
 @section('js')
 @endsection