
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('css/change-status.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')

@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to('_admin/tableno') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Random Key</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="random_key">
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn green" name="action_id" value="1">CALL</button>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn green" name="action_id" value="2">BILL</button>
                        </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-4"></div>
                            <div class="col-md-1">
                                <button type="submit" class="btn red"  name="deleted_at" >CANCEL</button>
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn red"  name="deleted_at" >CANCEL</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('js/change-status.js') }}
@endsection
@section('more-script')
@endsection