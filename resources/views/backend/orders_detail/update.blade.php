<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$orders_id = Input::get('orders_id');

// if(isset($data)) {
//     foreach($fillable as $value){
//         $$value = $data->$value;
//     }
// } else {
//     foreach($fillable as $value){
//         $$value = "";
//     }
// }
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    <a href="{{ url()->to('_admin/orders')}}">Order</a> | {{ $page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <?php
                            $data_order = DB::table('orders')
                                        ->where('orders_id',Input::get('orders_id'))
                                        ->get();
                        ?>
                        <thead>
                        @foreach($data_order as $key => $orders)
                            <tr>
                                <td colspan="2">
                                    <p><b>Order No. : </b>{{ $orders->orders_no }} </p>
                                    <p><b>Order Date. : </b>{{ $obj_fn->format_date_en($orders->created_at,8)}} </p>
                                    <p><b>Payment Date. : </b>{{ date('d F Y', strtotime($orders->payment_date))}} </p>
                                </td>
                                <td colspan="4"></td>
                            </tr>
                        @endforeach
                        <tr>
                            <th class="text-center col-sm-1">ID</th>
                            <th class="text-center col-sm-5" >Menu Name</th>
                            <th class="text-center col-sm-2" >Price</th>
                            <th class="text-center col-sm-2" >Option Price</th>
                            <th class="text-center col-sm-1" >Quantity</th>
                            <th class="text-center col-sm-2" >Total</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $totalprice = 0; $discount = 0; $code = 0;?>
                            @foreach($order as $key => $field)
                                <?php 
                                    $orders_detail = DB::table('orders_detail')
                                        ->leftJoin('orders_detail_option','orders_detail.orders_detail_id', '=', 'orders_detail_option.orders_detail_id')
                                        ->where('orders_detail_option.orders_detail_id', $field->orders_detail_id)
                                        ->first()->option_name;
                                    $total = ($field->price + $field->option_price) * $field->qty;
                                    $totalprice += $total;
                                    $discount = $field->discount_price;
                                    $code = $field->code;
                                ?>
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td><p>{{ $field->Menu($field->menu_id,$main_lang) }}</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp; {{ $orders_detail }} </p>
                                    </td>
                                    <td class="text-right">{{ number_format($field->price,2) }}</td>
                                    <td class="text-right">{{ number_format($field->option_price,2) }}</td>
                                    <td class="text-center">{{ number_format($field->qty) }}</td>
                                    <td class="text-right">{{ number_format($total,2) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" class="text-right"><b>Sub Price</b></td>
                                <td class="text-right">{{ number_format($totalprice,2) }}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right"><b>Discount </b>[<b style="color: red;"> {{ $code }} </b>]</td>
                                <td class="text-right">-{{ number_format( $discount,2) }}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right"><b>Grand Total</b></td>
                                <td class="text-right">{{ number_format($totalprice - $discount,2) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/admin/validation.js') }}
    {{ Html::script('js/backend/admin/more.js') }}
@endsection