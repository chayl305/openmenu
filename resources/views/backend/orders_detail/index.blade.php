<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');
$orders_id = Input::get('orders_id');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">ID</th>
                            <th class="text-center col-sm-1">Orders ID</th>
                            <th class="text-center col-sm-2">Menu Name</th>
                            <th class="text-center col-sm-1">Menu Option</th>
                            <th class="text-center col-sm-1">Quantity</th>
                            <th class="text-center col-sm-1">Price</th>
                            <th class="text-center col-sm-1">Option Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                            <?php $totalprice = 0; $discount = 0;?>
                                @foreach($data as $key => $field)
                                    <?php 
                                        $total = $field->price * $field->qty;
                                        $totalprice += $total;
                                        $discount = $field->discount_price;
                                    ?>
                                    <tr>
                                        <td class="text-center">{{ $field->$primaryKey }}</td>
                                        <td class="text-center">{{ $field->orders_id }}</td>
                                        <td>{{ $field->Menu($field->menu_id,$main_lang) }} {{ $field->total_price }}</td>
                                        <td class="text-center">{{ $field->menu_option }}</td>
                                        <td class="text-center">{{ number_format($field->qty) }}</td>
                                        <td class="text-right">{{ number_format($field->price,2) }}</td>
                                        <td class="text-right">{{ $field->option_price }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="5" class="text-right"><b>Sub Price</b></td>
                                    <td class="text-right">{{ number_format($totalprice,2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right"><b>Discount</b></td>
                                    <td class="text-right">{{ number_format( $discount,2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right"><b>Grand Total</b></td>
                                    <td class="text-right">{{ number_format($totalprice - $discount,2) }}</td>
                                </tr>
                            @else
                                <tr>
                                    <td class="text-center" colspan="10">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection