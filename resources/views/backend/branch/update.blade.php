<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$shop_id= Input::get('shop_id');

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {{ Html::style('assets/global/css/plugins.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
    {{ Html::style('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">

                    @if(!empty(Input::get('shop_id')))
                        <input type="hidden" name="shop_id" value="{{ Input::get('shop_id') }}">
                    @endif

                    <div class="form-body">
                        @if(empty(Input::get('shop_id')))
                            @if(empty(session()->get('s_shop_id')))
                                @if($txt_manage != 'Update')
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Site</label>
                                        <div class="col-md-4">
                                            <select name="shop_id" class="form-control select2 select2me" required>
                                                <option value="">Select......</option>
                                                @foreach($data_shop as $value)
                                                    <option value="{{ $value->shop_id }}" @if($value->shop_id == $shop_id) selected @endif >{{ $value->shop_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endif
                        @foreach($available_lang as $value)
                            <div class="form-group">
                                <label class="control-label col-md-3">Zone Name [ {{ strtoupper($value['lang']) }} ]</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="branch_name[{{ $value['lang'] }}]" @if($method=='PUT' and !empty($branch_name[$value['lang']])) value="{{ $branch_name[$value['lang']] }}" @endif required>
                                </div>
                            </div>
                        @endforeach
                        @if($txt_manage != 'Update')
                            <div class="form-group">
                                <label class="control-label col-md-3">Number of table</label>
                                <div class="col-md-4">
                                <input type="number" class="form-control" name="number_of_table" value="" required min="1">
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
    {{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}
    {{ Html::script('js/change-status.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/theme_component/components-pickers.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools2.js') }}
    {{ Html::script('js/backend/validation.js') }}
@endsection
