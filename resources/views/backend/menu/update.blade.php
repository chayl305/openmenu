<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;
$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$shop_id = input::get('shop_id');
if(!empty(input::get('category_id'))){
    $category_id = "?category_id=".input::get('category_id');
}else{
    $category_id = "";
}

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
    foreach($available_lang as $value){
        $menu_description[$value['lang']] = "";
    }

}


?>
@extends('backend.layout.main-layout')
@section('page-style')
{{ Html::style('assets/global/plugins/select2/select2.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
{{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
<div class="col-md-12">
    <div class="portlet light">
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                <input type="hidden" name="_method" value="{{ $method }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="str_param" value="{{ $str_param }}">
                @if(!empty(Input::get('shop_id')))
                <input type="hidden" name="shop_id" value="{{ Input::get('shop_id') }}">
                @endif
                <div class="form-body">
                    @foreach($available_lang as $value)
                        <div class="form-group">
                            <label class="control-label col-md-3">Menu Name [ {{ strtoupper($value['lang']) }} ]</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="menu_name[{{ $value['lang'] }}]" @if($method=='PUT' and !empty($menu_name[$value['lang']])) value="{{ $menu_name[$value['lang']] }}" @endif required="">
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    @endforeach 

                    @if(empty(Input::get('category_id')))
                        <div class="form-group">
                            <label class="control-label col-md-3">Category</label>
                            <div class="col-md-4">
                                <select name="category_id" class="form-control select2 select2me" required>
                                    <option value="">Select......</option>
                                    @foreach($data_category as $value)
                                    <option value="{{ $value->category_id }}" @if($value->category_id == $category_id) selected @endif >{{ $value->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    @foreach($available_lang as $value)
                        <div class="form-group">
                            <label class="control-label col-md-3">Description [ {{ strtoupper($value['lang']) }} ]</label>
                            <div class="col-md-7">
                                <textarea  rows="10" class="form-control" name="menu_description[{{ $value['lang'] }}]">{{$menu_description[$value['lang']]}}</textarea>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    @endforeach

                    <div class="form-group">
                        <label class="control-label col-md-3">Price</label>
                        <div class="col-md-2">
                            <input type="number" class="form-control" name="price" min="1.00" step="1.00" value="{{ $price }}" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Special Price</label>
                        <div class="col-md-2">
                            <input type="number" class="form-control" name="special_price"  min="0" value="{{ $special_price }}">
                        </div>
                    </div>

                    <?php
                    $field = 'img_name';
                    $label = 'Image';
                    $upload_path = 'menu';
                    ?>
                    <div id="bg-img">
                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-9">
                                <div class="img_thumbnail">
                                    @if($$field != '')
                                    <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                    @else
                                    <img src="" class="img_path" style="max-width:400px;">
                                    @endif

                                    <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">{{ $label }}</label>
                            <div class="col-md-4">
                                @if($$field != '')
                                <div class="btn btn-danger remove_img" >Remove</div>
                                <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                @elseif ($$field == '')
                                <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                @endif

                                <input type="hidden" name="img_del" class="img_del" value="n">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> </label>
                            <div class="col-md-4">
                                <span style="color:#F00">* หมายเหตุ</span><br>
                                <span style="">
                                    - รูปควรมีขนาด 800 x 460 pixel. <br>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">{{ $txt_manage }}</button>
                            <button type="reset" class="btn default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>

</div>
@endsection
@section('page-plugin')
{{ Html::script('assets/global/plugins/select2/select2.min.js') }}
{{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
{{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
{{ Html::script('js/backend/menu/validation.js') }}
@endsection