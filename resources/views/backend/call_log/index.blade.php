<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
{{ Html::style('css/change-status.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
{{ $page_title }}
@endsection
@section('page-content')
<div class="col-md-12">
    <div class="portlet light">
        <div class="form-search">
            <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
            </form>
            <hr>
        </div>
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-database font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">

                @if($count_data > 0)
                @foreach($data as $key => $field)
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>

                         <th class="text-center col-sm-1">Table No.</th>
                         <th class="text-center col-sm-4" >Zone</th>
                         <th class="text-center col-sm-4" >Action</th>
                         <th class="text-center col-sm-4" >Time Call</th>
                         <th></th>
                     </tr>
                 </thead>
                 <tbody>
                    <tr>
                        <td class="text-center">{{ $field->table_no }}</td>
                        <td class="text-center">{{ $field->branch_name }}</td>
                         @if($field->action_id == 1)
                        <td><div class="text-center"><p>Call Waiter</p></div>
                         @else
                            <td><div class="text-center"><p>Request Bill</p></div>
                        @endif
                        <td class="text-center">{{ $field->time_call }}</td>
                        <th class="text-center col-sm-2 col-md-2">
                            <form action="{{ url()->to('_admin/finishtime') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{csrf_field()}}
                                <input type="hidden" name="finish" value="{{$field->$primaryKey}}">
                                <button type="submit" class="btn btn-circle red btn-xs" name="finish_time><i class="fa fa-ban"></i>Cancel</button> 
                            </form>
                        </th>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td class="text-center" colspan="12">No Result.</td>
                    </tr>
                    @endif

                </tbody>
            </table>
        </div>
        {!! $data->appends(Input::except('page'))->render() !!}
    </div>
</div>
</div>

@endsection

@section('page-plugin')
{{ Html::script('js/change-status.js') }}
@endsection
@section('more-script')
@endsection