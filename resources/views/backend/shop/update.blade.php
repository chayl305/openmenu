<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;
$owner_id = Input::get('owner_id');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    @if(!empty(Input::get('owner_id')))
                        <input type="hidden" name="owner_id" value="{{ Input::get('owner_id') }}">
                    @endif
                    <div class="form-body">
                        @if(empty(Input::get('owner_id')))
                            @if(empty(session()->get('s_owner_id')))
                                <div class="form-group">
                                    <label class="control-label col-md-3">Company Name</label>
                                    <div class="col-md-4">
                                        <select name="owner_id" class="form-control" required>
                                            <option value="">Select.....</option>
                                            @foreach($owner as $value)
                                                <option value="{{ $value->owner_id }}" @if($value->owner_id == $owner_id) selected @endif >{{ $value->company_name }}</option>
                                            @endforeach
                                        </select >
                                    </div>
                                </div>
                            @endif
                        @endif
                        @foreach($available_lang as $value)
                            <div class="form-group">
                                <label class="control-label col-md-3">Site Name [ {{ strtoupper($value['lang']) }} ]</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="shop_name[{{ $value['lang'] }}]" @if($method=='PUT' and !empty($shop_name[$value['lang']])) value="{{ $shop_name[$value['lang']] }}" @endif required>
                                </div>
                            </div>
                        @endforeach
                        <div class="form-group">
                            <label class="control-label col-md-3">Business Type</label>
                            <div class="col-md-4">
                                <select name="business_type_id" class="form-control" required>
                                    <option value="">Select.....</option>
                                    @foreach($data_business as $value)
                                        <option value="{{ $value->business_type_id }}" @if($value->business_type_id == $business_type_id) selected @endif >{{ $value->name }}</option>
                                    @endforeach
                                </select >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Address</label>
                            <div class="col-md-4">
                                <textarea name="address" id="address" class="form-control ckeditor" data-error-container="#detail_error" style="height:150px;" required>{{ $address}}</textarea>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="phone" value="{{ $phone }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Website</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="website" value="{{ $website }}">
                            </div>
                        </div>
                        @if($txt_manage != 'Update')
                            <div class="form-group">
                                <label class="control-label col-md-3">Username</label>
                                <div class="col-md-4">
                                    <div class="input-group" style="text-align:left">
                                        <input type="text" class="form-control" name="username" id="username_input" value="">
                                        <span class="input-group-btn">
                                        <a href="javascript:;" class="btn green" id="username_checker">
                                            <i class="fa fa-check"></i> Check </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Password</label>
                                <div class="col-md-4">
                                    <input type="password" class="form-control" name="password" value="">
                                </div>
                            </div>
                        @endif
                        <?php
                            $field = 'img_logo';
                            $label = 'Image Logo';
                            $upload_path = 'shop';
                        ?>
                        <div id="bg-img">
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="img_thumbnail">
                                        @if($$field != '')
                                            <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                        @else
                                            <img src="" class="img_path" style="max-width:400px;">
                                        @endif

                                        <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">{{ $label }}</label>
                                <div class="col-md-4">
                                    @if($$field != '')
                                        <div class="btn btn-danger remove_img" >Remove</div>
                                        <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                    @elseif ($$field == '')
                                        <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                        <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                    @endif

                                    <input type="hidden" name="img_del" class="img_del" value="n">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-4">
                                    <span style="color:#F00">* หมายเหตุ</span><br>
                                        <span style="">
                                            - รูปควรมีขนาด 266 x 57 pixel. <br>
                                        </span>
                                </div>
                            </div>
                        </div>
                        @if($txt_manage != 'Update')
                            <hr>
                            <h3>FIRST ZONE</h3>
                            @foreach($available_lang as $value)
                                <div class="form-group">
                                    <label class="control-label col-md-3">Zone Name [ {{ strtoupper($value['lang']) }} ]</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="branch_name[{{ $value['lang'] }}]" value="A" @if($method=='PUT' and !empty($branch_name[$value['lang']])) value="{{ $branch_name[$value['lang']] }}" @endif required>
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                <label class="control-label col-md-3">Number of table</label>
                                <div class="col-md-4">
                                <input type="number" class="form-control" name="number_of_table" value="20" required min="1">
                                </div>
                            </div>
                        @endif
                        <hr>
                        <h3>CONTACT INFO</h3>
                        <div class="form-group">
                            <label class="control-label col-md-3">Contact Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="contact_name" value="{{ $contact_name }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Contact Phone</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="contact_phone" value="{{ $contact_phone }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Contact Email</label>
                            <div class="col-md-4">
                                <input type="email" class="form-control" name="contact_email" value="{{ $contact_email }}" required >
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green" @if($txt_manage == 'Add') disabled @endif>{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/validation.js') }}
    {{ Html::script('js/backend/admin/more.js') }}
@endsection