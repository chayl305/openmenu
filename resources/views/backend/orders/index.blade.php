<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    @if(empty(session()->get('s_shop_id')))
                        <div class="form-group">
                            <label class="control-label col-md-2">Site name</label>
                            <div class="col-md-3">
                                <select name="shop_id" id="shop_id" class="form-control select2 select2me">
                                    <option value="">Select...</option>
                                    @foreach($site as $field1)
                                        <option value="{{$field1->shop_id}}" @if($field1->shop_id == Input::get('shop_id')) selected @endif >{{$field1->shop_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="control-label col-md-2">Order by Date</label>
                        <div class="col-md-5">
                            <div class="input-group input-daterange col-md-12" data-date-format="yyyy-mm-dd">
                                <input type="type" class="form-control form_sql_datetime" name="from_date" value="{{ Input::get('from_date') }}">
                                <span class="input-group-addon"> to </span>
                                <input type="type" class="form-control form_sql_datetime" name="to_date" value="{{ Input::get('to_date') }}">
                            </div>
                            <span class="help-block">Select date range </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Search</label>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                            <span class="help-block">Search by Order No</span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-3 ">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-2">{!! $obj_fn->sorting('Orders No','orders_no',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-2">{!! $obj_fn->sorting('Orders Date','created_at',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-3">Site</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('Total Price','total_price',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">Status</th>
                            <th class="text-center col-sm-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="text-center">{{ $key+1 }}</td>
                                        <td class="text-center">{{ $field->orders_no }}</td>
                                        <td class="text-center">{{ $obj_fn->format_date_en($field->created_at,8) }}</td>
                                        <td>
                                            @if(empty(session()->get('s_shop_id')))
                                                <p><b>Site name : </b>{{ $field->Shop($field->shop_id,$main_lang) }}</p>
                                            @endif
                                            <p><b>Zone : </b>{{ $field->Branch($field->branch_id,$main_lang) }}</p>
                                            <p><b>Table no : </b>{{ $field->table_no }}</p>
                                        </td>
                                        <td class="text-right">{{ number_format($field->total_price-$field->discount_price,2) }}</td>
                                        <td class="text-center">
                                            @if($field->status == 0)
                                                <p style="color: blue;"><i class="fa fa-plus-circle"></i> New Menu</p>
                                            @elseif($field->status == 1)
                                                <p style="color: green;"><i class="fa fa-money"></i> ชำระเงินแล้ว</p>
                                            @elseif($field->status == 2)
                                                <p style="color: red;"><i class="fa fa-times" ></i> Cancel </p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                        <a href="{{ url()->to('_admin/orders_detail/create?'.$primaryKey."=".$field->$primaryKey) }}" class="btn btn-circle blue btn-xs"><i class="fa fa-search"></i> Order Detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="10">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
@endsection
@section('more-script')
    <script type="text/javascript">
        $(".form_sql_datetime").datetimepicker({
            isRTL: Metronic.isRTL(),
            format: "yyyy-mm-dd hh:ii:ss",
            autoclose: true,
            todayBtn: true,
            startDate: "2013-02-14 10:00:00",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            minuteStep: 10
        });
    </script>
@endsection