@extends('frontend.layout.main-layout')

@section('title', ' - Register')

@section('css')
  {!! Html::style('css/frontend/register.css') !!}
  {{ Html::style('assets/backend/pages/css/login.css') }}
  {{ Html::style('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}
  {{ Html::style('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}
  {{ Html::style('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}
  {{ Html::style('assets/global/plugins/uniform/css/uniform.default.min.css') }}
  <!-- END GLOBAL MANDATORY STYLES -->

  <!-- BEGIN PAGE LEVEL STYLES -->
  {{ Html::style('assets/backend/pages/css/login.css') }}
  <!-- END PAGE LEVEL STYLES -->

  <!-- BEGIN THEME STYLES -->
  {{ Html::style('assets/global/css/components-md.css') }}
  {{ Html::style('assets/global/css/plugins-md.css') }}
  {{ Html::style('assets/backend/layout3/css/layout.css') }}
  {{ Html::style('assets/backend/layout3/css/themes/default.css') }}
  {{ Html::style('assets/backend/layout3/css/custom.css') }}
@endsection

@section('content')
  <div id="shop">
    <div class="container">
      <div class="text-center">
        <h3>Register</h3>
        <form action="{{ url()->to('shop/register/check') }}" method="post" enctype="multipart/form-data" id="register-form">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
        </div>
          <div class="text-center">
            <div>
              <label>Shop Name [TH]</label>
              <input type="text" name="shop_name_th" id="" class="">
            </div>
            <div>
              <label>Shop Name [EN]</label>
              <input type="text" name="shop_name_en" id="" class=""></input>
            </div>
            <div>
              <label>website</label>
              <input type="text" name="website" id="" class=""></input>
            </div>
            <div>
              <label>Business Type</label>
              <select name="business_type">

                  <option value="1">1</option>
                  <option value="1">1</option>
                  <option value="1">1</option>
                  <option value="1">1</option>

              </select>
            </div>
            <div>
              <label>Number of table</label>
              <input type="number" name="number_of_table" id="" class=""></input>
            </div>
        </div>

          <div class="text-center">

            <h3>Company Info</h3>
            <div>
              <label>Company Name</label>
              <input type="text" name="company_name" id="" class=""></input>
            </div>
            <div>
              <label>Address</label>
              <textarea name="address" id="" class="" rows="10" cols="30"></textarea>
            </div>
            <div>

              <label>zone</label>
              <input type="text" name="zone" id="" class="" rows="10" cols="30">>
            </div>
            <div>
              <label>district</label>
              <input type="text" name="district" id="" class="" rows="10" cols="30">
            </div>
            <div>
              <label>province</label>
              <input type="text" name="province" id="" class="" rows="10" cols="30">
            </div>
            <div>
              <label>Zip</label>
              <input type="text" name="zip" id="" class="" rows="10" cols="30">
            </div>
            <div>
              <label>Phone</label>
              <input type="text" name="phone" id="" class=""></input>
            </div>

            <div>
              <label>website</label>
              <input type="text" name="website" id="" class=""></input>
            </div>

          </div>
          <div class="text-center col-md-6">
            <div>
              <label>Email</label>
              <input type="email" name="email" id="" class=""></input>
            </div>
            <div>
              <label>Password</label>
              <input type="password" name="password" id="" class=""></input>
            </div>
          </div>

          </div> -->
          <div>
            <input type="submit" name="" id="" class=""></input>
            <!-- <input type="button" name="" id="" class="" value="Cancel"></input> -->

          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('more-script')
{{ Html::script('assets/global/plugins/jquery-migrate.min.js') }}
        <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
{{ Html::script('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}
{{ Html::script('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}
{{ Html::script('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
{{ Html::script('assets/global/plugins/jquery.blockui.min.js') }}
{{ Html::script('assets/global/plugins/jquery.cokie.min.js') }}
{{ Html::script('assets/global/plugins/uniform/jquery.uniform.min.js') }}
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
{{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
<!-- END PAGE LEVEL PLUGINS -->

{{ Html::script('assets/global/scripts/metronic.js') }}
{{ Html::script('assets/backend/layout3/scripts/layout.js') }}
{{ Html::script('assets/backend/layout3/scripts/demo.js') }}

{{ Html::script('js/frontend/login/login.js') }}
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init(); // init demo features
    });
</script>
@endsection
