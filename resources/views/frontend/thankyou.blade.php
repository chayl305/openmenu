@extends('frontend.layout.main-layout')

@section('title', ' - Thank you')

@section('css')
  {!! Html::style('css/frontend/thank-you.css') !!}
  {!! Html::style('tools/bxslider/jquery.bxslider.css') !!}
@endsection

@section('content')
  <div class="row">
    <div class="container">
      <div class="thank-text text-center">
        <h1>Thank You For Register Open Menu Application.</h1>
        <h1>Plese Checked You Email for Information.</h1>
      </div>
    </div>
  </div>
@endsection
@section('script')
<!-- bxSlider Javascript file -->
<!-- {{ Html::script('tools/bxslider/jquery.bxslider.min.js') }} -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="../public/tools/bxslider/jquery.bxslider.min.js"></script>
<script>
      $(document).ready(function(){
        $('.bxslider').bxSlider({
          // mode: 'fade',
          controls: false,
          infiniteLoop: true,
          auto: true,
          speed: 300,
        });
        $('.feature-o').bxSlider({
          minSlides: 4,
          maxSlides: 4,
          moveSlides:1,
          slideWidth: 360,
          // slideMargin: 10
        });
      });
</script>
<!-- bxSlider CSS file -->

@endsection
