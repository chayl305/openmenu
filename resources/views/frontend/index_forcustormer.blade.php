@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')

  {!! Html::style('css/frontend/home.css') !!}
  {!! Html::style('tools/bxslider/jquery.bxslider.css') !!}
@endsection

@section('content')
  <div id="home">

      <div id="head-open-menu" class="container">
        <!-- <div class="text-head text-center">With OPEN MENU APP Your customer can call waiter, request bill, order food & drink with their own mobile device.</div> -->
        <div class="text-head text-center"><span>Easy to Use Food Ordering Application </span><span>at your restaurant, hotel or even at the beach bed</span></div>
        <!-- <div class="text-head text-center">Easy to Use Food Ordering Application at your restaurant, hotel or even at the beach bed</div> -->
        <div class="text-head-btn text-center" data-toggle="modal" data-target="#myModal"> <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>Watch Video</div>

      </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div> -->
      <div class="modal-body img-responsive">
        <!-- <iframe class="youtube"width="560" height="315" src="https://www.youtube.com/embed/1G4isv_Fylg" frameborder="0" allowfullscreen></iframe> -->
        <iframe class="youtube" src="https://www.youtube.com/embed/1G4isv_Fylg" frameborder="0" allowfullscreen></iframe>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
      <div id="get-app" >
        <div id="get-bg" class="container">
          <div class="col-xs-12 col-sm-4 col-md-4">
            <img id="img-phone-openmenu" class=""src="{{URL::asset('images/for_customer/phone-download-app.png')}}">
          </div>
          <div class="download col-xs-12 col-sm-6 col-md-6">
            <div id="download-head">Download our App</div>
            <div id="download-content">Enter your mobile phone number, we will send you download link for future use.</div>
            <!-- <p id="download-content">Enter your mobile phone number and we'll send you a download link.</p> -->
            <div class="col-md-8" id="input-email-download-app">
              <input class="" id=""type="email" name="email-download-app" style="">
            </div>
            <div class="col-md-4">
              <input class="down-app-btn" type="submit" value="Get The App">
            </div>
            <div class="col-md-12" id="download-content-footer">You can download application from App Store or Google Play Store by click the link below.</div>
            <div class="" >
              <!-- <div class="col-xs-4 col-md-4" > -->
                <img class="img-store col-xs-4 col-md-6" src="{{URL::asset('images/for_customer/banner-googleplay.png')}}">
              <!-- </div> -->
              <!-- <div class="col-xs-4 col-md-4" > -->
                <img class="img-store-apple col-xs-4 col-md-6" src="{{URL::asset('images/for_customer/banner-apple-app.png')}}">
              <!-- </div> -->
            </div>
          </div>
        </div>
      </div>



     <div id="how-it-work">
       <div class="container">
        <div id="how-content-head" class="text-center">How It Works</div>
        <div id="how-content">
              <!-- <div class="how-details col-md-3"> -->
              <a class="how-details-2">
                  <img class="img-responsive" src="{{URL::asset('images/for_customer/scan-qr.jpg')}}">
                <div class="overbox">
                  <div class="b">
                    Table When you are in a restaurant, Scan QR Code at the table to get connect!</div>
                </div>

                <!-- <p>Scan QR code at your table</p> -->
                <p>Scan QR code at your table</p>
              </a>
              <!-- </div> -->
              <a class="how-details-2">
                <img class="img-responsive" src="{{URL::asset('images/for_customer/call-waiter.jpg')}}">
                <div class="overbox">
                  <div class="b">Customer can click call waiter button to call waiter for a service. The order will be sent directly to waiter dashboard. click request bill button to ask for a bill. The order will be sent directly to cashier dashboard.</div>
              </div>
                <p>Call waiter Or Request Bill</p>
              </a>
              <a class="how-details-2">
                <img class="img-responsive" src="{{URL::asset('images/for_customer/food-category2.jpg')}}">
                <div class="overbox">
                  <div class="b">Diner get promotion offer. They can browse for recommended dishes in each categories and place order with their mobile phone.</div>
              </div>
                <p>Food Category</p>
              </a>
              <a class="how-details-2">
                <img class="img-responsive" src="{{URL::asset('images/for_customer/food-menu.jpg')}}">
                <div class="overbox">
                  <div class="b">
                    Diners can use their mobile browse for the restaurant’s recommended dishes. They can browse menu by categories and place order directly from their mobile phone.</div>
                </div>
                <p>Food Menu</p>
              </a>
        </div>
      </div>

      </div>

       <div class="view-menu" >
         <div class="container" >
            <div class="view-content col-md-8">
              <div class="view-detail-head">Menu on Mobile</div>
              <div class="view-detail">
                Diners can use their mobile browse for the restaurant’s<br>
                recommended dishes. They can browse menu by categories<br>
                and place order directly from their mobile phone.
              </div>
            </div>
            <div class="bx-size col-md-4">
              <ul class="bxslider">
                <img  src="{{URL::asset('images/for_customer/menuontable.png')}}">
                <!-- <img  src="{{URL::asset('images/for_customer/phone-menu.png')}}"> -->
              </ul>
            </div>
          </div>
      </div>


      <div class="payment-menu">
        <div class="container">
          <div class="bx-size  slider-position col-md-5">
            <ul class="bxslider">
              <img  src="{{URL::asset('images/for_customer/payment.png')}}">
              <!-- <img  src="{{URL::asset('images/for_customer/phone-menu.png')}}"> -->
            </ul>
          </div>
          <div class="payment-content col-md-7">
            <div class="payment-detail-head col-md-12">Payment</div>
            <div class="payment-detail">Pay with your credit card directly from your mobile save & sound.
            </div>
          </div>
        </div>
      </div>

      <div class="subscribe-news">
        <div class="container">
          <div class="subscribe">
            <div class="sub-head">Subscribe to Newsletter</div>
            <div class="sub-detail">Sign up with your email address to receive news and updates <br>straight to your inbox</div>
            <div class="col-md-8" id="input-email-sub">
              <input class=""  type="email" name="email-download-app">
            </div>
            <!-- <div class="subscribe-btn get-app-btn  text-center col-xs-4 col-md-4">Subscribe</div> -->
            <div class="col-md-2">
              <input class="subscribe-btn get-app-btn" type="submit" value="Subscribe">
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="subscribe-news">
        <div class="container">
        <div class="subscribe">
          <div class="sub-head">Subscribe to Newsletter</div>
          <div class="sub-detail">Sign up with your email address to receive news and updates <br>straight in your inbox</div>
          <div class="col-xs-12 col-md-8" id="input-email-sub">
            <input class=""  type="email" name="email-download-app">
          </div>
          <div class="subscribe-btn get-app-btn  text-center col-xs-4 col-md-4">Subscribe</div>
          <div class="col-xs-4 col-sm-2 col-md-2">
            <input class="subscribe-btn get-app-btn" type="submit" value="Subscribe">
          </div>
        </div>
      </div>
      </div> -->


  </div>
@endsection
@section('script')
<!-- bxSlider Javascript file -->
<!-- {{ Html::script('tools/bxslider/jquery.bxslider.min.js') }} -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="../public/tools/bxslider/jquery.bxslider.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
      $(document).ready(function(){

        $('.bxslider').bxSlider({
          // mode: 'fade',
          controls: false,
          infiniteLoop: true,
          auto: true,
          speed: 300,
          slideWidth: 260,
          // slideMargin: 200
          // easing: 'easeOutElastic',
        });
      });

</script>
<script>

</script>
<!-- bxSlider CSS file -->

@endsection
