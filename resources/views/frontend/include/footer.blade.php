<footer>
  <div class="container">
    <div class="need-help"><a href="#">Need Help?</a></div>
    <div class="contactus">Contact Us Or Call 1 (888) 707-2469</div>
    <hr>
    <div class="social-media">
      <a href="http://www.facebook.com/"><img  src="{{URL::asset('images/for_customer/logo-facebook.png')}}"></a>
      <a href="http://www.facebook.com/"><img  src="{{URL::asset('images/for_customer/logo-youtube.png')}}"></a>
      <a href="http://www.facebook.com/"><img  src="{{URL::asset('images/for_customer/logo-instagram.png')}}"></a>
    </div>
    <div class="copyright">
      &copy; Copyright 2016 Bangkok Solutions Co., Ltd.
    </div>
  </div>
  <!-- <div class="container"> -->
    {{--<div id="footer-copyright">--}}
      {{--<p id="need-help">NEED HELP?</p>--}}
      {{--<p class="text-center">CONTACT US OR CALL 1 (888) 707-2469</p>--}}
      {{--<hr style="width:360px;border-top: 1px solid #979797;"></hr>--}}
      {{--<div class="social">--}}
        {{--<a href="http://www.facebook.com/"><img  src="{{URL::asset('images/for_customer/logo-facebook.png')}}"></a>--}}
        {{--<a href="http://www.facebook.com/"><img  src="{{URL::asset('images/for_customer/logo-youtube.png')}}"></a>--}}
        {{--<a href="http://www.facebook.com/"><img  src="{{URL::asset('images/for_customer/logo-instagram.png')}}"></a>--}}
      {{--</div>--}}
      {{--<p class="text-center" style="margin-top:10px;">© COPYRIGHT 2016 BANGKOK SOLUTIONS CO. LTD.</p>--}}
    {{--</div>--}}
    <!-- Copyright &copy; 2016. All Right Reserved. -->
  <!-- </div> -->
</footer>
