<header>
  <div class="container">
    <div id="nav-logo">
      <a href="{{ URL::route('home') }}" class="logo"><img src="{{URL::asset('images/for_customer/logo-open-menu-white.png')}}"></a>
    </div>

    <div id="nav-menu">
      <ul class="nav">
        <li class="nav-item"><a class="menu" href="{{ URL::route('home') }}">For Restaurant</a></li>
        <li class="nav-item"><a class="menu" href="{{ URL::route('for-customer') }}">For Customer</a></li>
        <li class="nav-item"><a class="lang @if($lang == 'th') active @endif" href="{{ url()->to('lang/th') }}" >TH</a> | <a class="lang @if($lang == 'en') active @endif" href="{{ url()->to('lang/en') }}" >EN</a></li>
      </ul>
    </div>
  </div>
</header>
