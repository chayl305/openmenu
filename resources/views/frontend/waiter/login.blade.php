@extends('frontend.layout.main-layout')

@section('title', ' - Login')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
  <div id="content">
    <div class="container">
      <div class="text-center"> <h1>Waiter Login</h1> </div>
      <div class="">
          <form method="POST" action="{{url()->to('waiter/login/check')}}" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="form-group text-center" >
                <?php $field='username_login' ?>
                  <label>Username</label>
                  <input type="username" name="username_login" >
              </div>
              <div class="form-group text-center" >
                <?php $field='password' ?>
                  <label>Password</label>
                  <input type="password" name="password" >                  
              </div>
              <div class="form-group text-center" >
                  <input type="submit" name="submit" >
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
@endsection
