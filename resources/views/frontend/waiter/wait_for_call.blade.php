@extends('frontend.layout.main-layout')

@section('title', ' - WaitForCall')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
  <div id="content">
    <div class="container">
      <div class="text-center"> <h1>Waitting</h1> </div>
      {{ session()->get('waiter_name') }}

    </div>
  </div>
@endsection
