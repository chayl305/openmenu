<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Confirm Email</title>
</head>
<body>


<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
  <div>Welcome To OpenMenu Application</div>
  <div>Thank you for Register</div>
  <div>Name : {{$adminDB->firstname}}</div>
  <div>Username : {{$adminDB->username}}</div>
  <div>Password : {{$adminDB->password}}</div>

  <?php
    $lang = session()->get('locale');
    $business_type_lang = App\Models\BusinessTypeTr::where('business_type_id', $shopDB->BusinessType)->where('lang', $lang)->first();
    $branch_tr = App\Models\BranchTr::where('branch_id', $gen_branch->branch_id)->get();
    $shop_tr = App\Models\ShopTr::where('shop_id', $adminDB->shop_id)->get();
    $tables = App\Models\Tables::where('branch_id', $gen_branch->branch_id)->get();
    // $branch_tr = App\Models\Waiter::where('waiter_id', $gen_waiter->waiterd)->get();

  ?>
  <div>BusinessType : </div>
  @foreach( $branch_tr as $branch )
    @if($branch->lang == 'th')
      <div>Branch TH : {{$branch->branch_name}}</div>
    @elseif($branch->lang == 'en')
      <div>Branch EN : {{$branch->branch_name}}</div>
    @endif
  @endforeach
  @foreach($shop_tr as $shop)
    @if($shop->lang == 'th')
      <div>Shop Name TH: {{$shop->shop_name}}</div>
    @elseif($shop->lang == 'en')
      <div>Shop Name EN: {{$shop->shop_name}}</div>
    @endif
  @endforeach
  <div>Shop Address: {{$shopDB->address}}</div>
  <div>Shop Contact Name: {{$shopDB->contact_name}}</div>
  <div>Shop Contact Phone: {{$shopDB->contact_phone}}</div>
  <div>Shop Address: {{$shopDB->address}}</div>
  <div>Waiter Name : {{$gen_waiter->waiter_name}}</div>
  <div>Waiter Username : {{$gen_waiter->username}}</div>
  <div>Waiter Password : {{$gen_waiter->password}}</div>
  <div>Kitchen Name : {{$gen_waiter->waiter_name}}</div>
  <div>Kitchen Username : {{$gen_waiter->username}}</div>
  <div>Kitchen Password : {{$gen_waiter->password}}</div>
  @foreach($tables as $table)
    <div>Table No.: {{$table->table_no}}</div>
  @endforeach

  <div>

  </div>
  <div></div>
  <div></div>

</div>

</body>
</html>
