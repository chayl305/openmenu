@extends('frontend.layout.main-layout')

@section('title', ' - For Restuarent')

@section('css')
  {!! Html::style('tools/bxslider/jquery.bxslider.css') !!}
  {!! Html::style('css/frontend/for-restuarent.css?v=3.2.3') !!}
@endsection

@section('content')
  <div id="home">
    <div id="head-open-menu">
      <div class="container">
        <div class="head-text text-center">
          Your customer can call waiter, request bill, order food & drink with their own mobile device.
          <!-- Mobile Application that your customer can call waiter, request bill, <br>order food with their mobile -->
        </div>
        <div class="how-its-work text-center">
          <a href="#" class="head-btn"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span> How Its work</a>
        </div>
      </div>
    </div>

    <div id="features">
      <div class="container">
        <div class="features-head text-center">Features</div>
        <div class="features-slider">
          <ul class="slide">
            <li>
              <a href="#" class="item">
                <div class="hover">
                  <span>How difficult to get waiter attention in the restaurant rush hours. We ease the pain by helping you call waiter in a button, only one click and the waiter will come to serve you ASAP.</span>
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-1.jpg')}}" >
              </a>
              Call Waiter
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                  <span>Whenever you are ready to go and want to call for a bill. Just click ಯRequest Billರ. The order will be sent directly to cashierಬs dashboard. No more waiting to call for a waiter to call COMfor a bill for you.</span>
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-1.jpg')}}" >
              </a>
              Request Bill
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                  Present your food as eye candy on digital menu screen. You can customize menu with as many categories as you want.
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-2.jpg')}}" >
              </a>
              Digital Menu
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                  Restaurant can make online plan for promotions & offers to update on mobile application any time they want. Customer will see the new promotion and can apply the promotion when order in real time.
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-3.jpg')}}" >
              </a>
               Promotions & offers
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                  Customer can select what they want to order and place order online directly with their mobile phone. This will reduce human error in order taking and customers will get.
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-4.jpg')}}" >
              </a>
              Mobile Ordering
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                  Protect your customers with mobile payment. No one in your restaurant touch, see and collect your customer credit card information.
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-1.jpg')}}" >
              </a>
              Mobile Payment
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                Present your food as eye candy on digital menu screen. You can customize menu with as many categories as you want.
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-2.jpg')}}" ></a>
                Digital Menu
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                  Restaurant can make online plan for promotions & offers to update on mobile application any time they want. Customer will see the new promotion and can apply the promotion when order in real time.
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-3.jpg')}}" ></a>
                Online Ordering
            </li>
            <li>
              <a href="#" class="item">
                <div class="hover">
                Protect your customers with mobile payment. No one in your restaurant touch, see and collect your customer credit card information.
                </div>
                <img src="{{URL::asset('images/for_restuarent/feature-4.jpg')}}" ></a>
                Payment
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div id="what-you-get">
      <div class="container">
        <div class="text-head text-center">What you get</div>
        <div class="what-you-get-slider">
          <ul class="what-slide">
            <li class="item">
              <div class="text-content">
                <span class="text">
                  <div class="head">Waiter Display System</div>
                      Show new orders, order served. Show table numbers that call for services and request bills.
                  </span>
              </div>
              <img src="{{URL::asset('images/for_restuarent/img-what-you-get.png')}}">
            </li>
            <li class="item">
              <div class="text-content">
                <span class="text">
                  <div class="head">Kitchen Display System</div>
                      Show new orders, processing orders and ready to serve orders.
                  </span>
              </div>
              <img src="{{URL::asset('images/for_restuarent/img-what-you-get.png')}}">
            </li>

            </ul>
          </div>
        </div>
      </div>

    <div id="benefit">
      <div class="container">
        <div class="benefit-head">Benefit</div>
        <div class="benefit-item row">
          <div class="col-xs-6 col-md-3">
            <img class="benefit-img"src="{{ URL::asset('images/for_restuarent/Fast-Services.png') }}">
            <p>Fast Services</p>
          </div>
          <div class="col-xs-6 col-md-3">
            <img class="benefit-img" src="{{ URL::asset('images/for_restuarent/Reduce-Human-Error.png') }}">
            <p>Reduce Human Error</p>
          </div>
          <div class="col-xs-6 col-md-3">
            <img class="benefit-img" src="{{ URL::asset('images/for_restuarent/Customer-Satisfactionl.png') }}">
            <p>Customer Satisfaction</p>
          </div>
          <div class="col-xs-6 col-md-3">
            <img class="benefit-img" src="{{ URL::asset('images/for_restuarent/Increase-Sales.png') }}">
            <p>Increase Sales</p>
          </div>
        </div>
      </div>
    </div>

    <div id="about">
      <div class="container">
        <div class="about-head">About Open Menu</div>
        <div class="about-content">
              OpenMenu enhances the diner's experience together with the restaurant management. Customers can call waiter for services on their mobile phone. Get attention from waiter in no time. Customers can also browse
              menu from the app and place the order. No need to ask for menu and wait for waiter to take order from you. OpenMenu will reduce human error and improve customer satisfaction – you get exactly what you order.
        </div>
      </div>
    </div>

    <div id="register">
      <div class="container">
        <div class="register-head">Implement Openmenu to engage customers & watch for sales growth</div>
        <div class="register-form">
          <?php
            $col_label = "col-md-4";
            $col_input = "col-md-8";
          ?>
          <form class="form-horizontal" action="{{ url()->to('shop/register/check') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Shop Name [TH]</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="shop_name_th" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Shop Name [EN]</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="shop_name_en">
                  </div>
                </div>

                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Business Type</label>
                  <div class="{{ $col_input }}">
                    <select class="form-control" name="business_type">
                      @foreach($business_type as $type)
                          <option>{{$type->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Username</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="username" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Password</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="password" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Number of Table</label>
                  <div class="{{ $col_input }}">
                    <input type="number" class="form-control" name="number_of_table" >
                  </div>
                </div>
              </div>
            </div>
            <hr>

            <div class="row">
              <div class="col-sm-6">
                <div class="head-text">Company Info</div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Company Name</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="company_name">
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Address</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="company_address">
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Phone</label>
                  <div class="{{ $col_input }}">
                    <input type="number" class="form-control" name="company_phone">
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Email</label>
                  <div class="{{ $col_input }}">
                    <input type="email" class="form-control" name="company_email">
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Website</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="company_website">
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="head-text">Contact Info</div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Contact Name</label>
                  <div class="{{ $col_input }}">
                    <input type="text" class="form-control" name="contact_name">
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Contact Phone</label>
                  <div class="{{ $col_input }}">
                    <input type="number" class="form-control" name="contact_phone">
                  </div>
                </div>
                <div class="form-group">
                  <label class="{{ $col_label }} control-label">Contact Email</label>
                  <div class="{{ $col_input }}">
                    <input type="email" class="form-control" name="contact_email">
                  </div>
                </div>
              </div>

            </div>

            <div class="lasttext">
              While in the first instance you can only print up to 20 codes here more can be added, at no charge, later from your personal dashboard. Use your email address and password to log into the dashboard, from here you can modify existing codes, add and print single or specific ranges of codes e.g. 101-145, 185-225 as well as ordering professional tableware from our online store that matches your format choice.
            </div>

            <div class="form-submit">
              <!-- <a href="#" class="btn-submit">Sign Up</a> -->
              <button class="btn-submit">Sign Up</button>
              <!-- <input type="submit" class="btn-submit">Sign up</input> -->
            </div>

          </form>
        </div>

      </div>
    </div>

      {{--<div class="application-form container">--}}
        {{--<form action="{{ url()->to('shop/register/check') }}" method="post" enctype="multipart/form-data" id="register-form">--}}
          {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--<div id="app-header">Implement Openmenu tp engage customers & watch for sales growth</div>--}}
        {{--<div class="form-group">--}}
          {{--<div class="form-text  col-xs-12  col-md-2">Shop Name[TH]</div>--}}
          {{--<div class="form-input  col-xs-12  col-md-4"><input type="text" name="shop_name_th" id="" class="form-control"></div>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
          {{--<div class="form-text col-xs-12 col-md-2">Shop Name[EN]</div>--}}
          {{--<div class="form-input  col-xs-12 col-md-4"><input type="text" name="shop_name_en" id="" class="form-control"></div>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
          {{--<div class="form-text col-xs-12  col-md-2">Business Type</div>--}}
          {{--<div class="form-input  col-xs-12  col-md-4">--}}
            {{--<select name="business_type">--}}
                {{--<option value="">Example...</option>--}}
                {{--<option value="">Example...</option>--}}
                {{--<option value="">Example...</option>--}}
                {{--<option value="">Example...</option>--}}
                {{--<option value="">Example...</option>--}}
                {{--<option value="">Example...</option>--}}
            {{--</select>--}}
          {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
          {{--<div class="form-text col-xs-12  col-md-2">Number of table</div>--}}
          {{--<div class="form-input col-xs-12 col-md-4"><input type="number" name="number_of_table" id="" class="form-control"></input></div>--}}
        {{--</div>--}}
        {{--<hr class="col-md-12" style="width:90%;border-top: 1px solid #979797; margin-top:20px;"></hr>--}}
        {{--<div class="company col-md-12">--}}
          {{--<div id="company-text"> Company Info </div>--}}
          {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
              {{--<div class="form-text col-xs-12 col-md-4">Company Name</div>--}}
              {{--<div class="form-input col-xs-12 col-md-8"><input type="text" name="company_name" id="" class="form-control"></input></div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
              {{--<div class="form-text col-xs-12 col-md-4">Email</div>--}}
              {{--<div class="form-input col-xs-12 col-md-8"><input type="email" name="email" id="" class="form-control"></div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
              {{--<div class="form-text col-xs-12 col-md-4">Password</div>--}}
              {{--<div class="form-input col-xs-12 col-md-8"><input type="password" name="password" id="" class="form-control"></div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
              {{--<div class="form-text col-xs-12 col-md-4">Comfirm Password</div>--}}
              {{--<div class="form-input col-xs-12 col-md-8"><input type="password" name="confirm_password" id="" class="form-control"></div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
              {{--<div class="form-text  col-xs-12 col-md-4">Phone</div>--}}
              {{--<div class="form-input col-xs-12 col-md-8"><input class="form-control" type="number" name="phone"></div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
              {{--<div class="form-text col-xs-12 col-md-4">Address</div>--}}
              {{--<div class="form-input col-md-8"><textarea name="address" id="" class="" rows="6" cols="30"></textarea></div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<div class="term-checkbox col-md-12"><input type="checkbox"> I agree to the Open Menu terms and condition that I have read here</div>--}}
        {{--</div>--}}

      {{--</div>--}}

      {{--<div><button id="sign-up-btn" class="text-center">Sign Up</button></div>--}}
    {{--</form>--}}
  </div>
@endsection
@section('script')
  <!-- bxSlider Javascript file -->
  {{ Html::script('tools/bxslider/jquery.bxslider.min.js') }}
<script>
  $(document).ready(function(){
    $('#features .hover').on('click',function(e){
      e.preventDefault();
    });
    function optionSlider(){
      var windowWidth = $(window).width();
      if (windowWidth > 991 ) numItem = 4;
      else if (windowWidth > 767 && windowWidth <= 991 ) numItem = 3;
      else if (windowWidth <= 767) numItem = 1;
      optionBx = {
        easing : 'linear',
        auto : true,
        controls : false,
        caption : true,
        slideMargin : 10,
        minSlides: numItem,
        maxSlides: numItem,
        moveSlides:1,
        slideWidth: 360,
        pager: false,
      }
      return optionBx;
    }
    optionBx = optionSlider();
    $('.slide').bxSlider(optionBx);

    $('.what-slide').bxSlider({
      easing : 'linear',
      auto: true,
      controls: false,
      infiniteLoop: true,
      slideMargin : 0,
      pager: false,
    });

  });
</script>
@endsection
